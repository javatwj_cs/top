package com.yc.portal;

import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import com.yc.common.util.KryoRedisSerializer;
import com.yc.common.util.KryoSerializer;
import com.yc.core.AppConfig;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.session.ExpiringSession;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.security.SpringSessionBackedSessionRegistry;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;
import org.springframework.util.StringUtils;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 项目配置。
 *
 */
@SpringBootApplication
@ServletComponentScan
@EnableWebSecurity
@EnableAsync(proxyTargetClass = true)
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 60 * 30)
@Import(AppConfig.class)
@EnableSwagger2
public class PortalApplication {

	@Autowired
	private Environment env;

	/**
	 * 项目入口。
	 *
	 * @param args 参数
	 */
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(PortalApplication.class);
		app.setBannerMode(Banner.Mode.LOG);
		app.run(args);
	}

	/**
	 * 覆盖Spring默认Session序列化类。
	 *
	 * @param kryoSerializer KryoSerializer
	 * @return 实例
	 */
	@Bean
	public RedisSerializer<Object> springSessionDefaultRedisSerializer(KryoSerializer kryoSerializer) {
		return new KryoRedisSerializer(kryoSerializer);
	}

//	/**
//	 * 请求次数阈值过滤器。
//	 *
//	 * @return 实例
//	 */
//	@Bean
//	public FilterRegistrationBean requestThresholdFilter() {
//		int rate = env.getRequiredProperty("threshold.rate", Integer.class);
//		TimeUnit rateUnit = env.getRequiredProperty("threshold.rate.unit", TimeUnit.class);
//		int punishDuration = env.getRequiredProperty("threshold.punish.duration", Integer.class);
//		TimeUnit punishUnit = env.getRequiredProperty("threshold.punish.unit", TimeUnit.class);
//		FilterRegistrationBean registration = new FilterRegistrationBean();
//		registration.setFilter(new RequestThresholdFilter(rate, rateUnit, punishDuration, punishUnit));
//		registration.addUrlPatterns("/*");
//		registration.setOrder(Integer.MIN_VALUE);   // 最高优先级
//		return registration;
//	}

	// /**
	// * 监听Session事件，SpringSession注册方式。
	// *
	// * @return 实例
	// */
	// @Bean
	// public SessionEventHttpSessionListenerAdapter sessionListenerAdapter(SessionListener sessionListener) {
	// return new SessionEventHttpSessionListenerAdapter(Arrays.asList(sessionListener));
	// }

	// /**
	// * Session策略。
	// *
	// * @return 实例
	// */
	// @Bean
	// public HttpSessionStrategy httpSessionStrategy() {
	// return new HeaderHttpSessionStrategy();
	// }

	/**
	 * SpringSession相关cookie设置。
	 *
	 * @return 实例
	 */
	@Bean
	public CookieSerializer cookieSerializer() {
		Integer cookieMaxAge = env.getProperty("server.session.cookie.max-age", Integer.class);
		DefaultCookieSerializer serializer = new DefaultCookieSerializer();
		serializer.setCookieName("SESS");
		if (cookieMaxAge != null) {
			serializer.setCookieMaxAge(cookieMaxAge);
		}
		return serializer;
	}

//	/**
//	 * SpringMVC配置。
//	 *
//	 * @return 实例
//	 */
//	@Bean
//	public ApplicationMvc applicationMvc() {
//		return new ApplicationMvc();
//	}
//
	/**
	 * SpringSecurity配置。
	 *
	 * @return 实例
	 */
	@Bean
	public ApplicationSecurity applicationSecurity() {
		return new ApplicationSecurity();
	}

	/* (non-Javadoc)
     * @see org.springframework.scheduling.annotation.AsyncConfigurer#getAsyncExecutor()
     */

	/**
	 * Swagger相关配置。
	 *
	 * @return 实例
	 */
	@Bean
	public Docket swaggerApi() {
		Boolean enable = env.getProperty("swagger.enable", Boolean.class, Boolean.FALSE);
		return new Docket(DocumentationType.SWAGGER_2).enable(enable).select().apis(RequestHandlerSelectors.any()).paths(PathSelectors.any()).build();
	}

//	/**
//	 * SpringMVC配置。
//	 */
//	private static class ApplicationMvc extends WebMvcConfigurerAdapter {
//
//		@Override
//		public void addInterceptors(InterceptorRegistry registry) {
//			registry.addInterceptor(new AuthenticationPrincipalHandler());
//		}
//
//	}

	/**
	 * SpringSecurity配置。
	 */
	@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
	private static class ApplicationSecurity extends WebSecurityConfigurerAdapter {

		@Value("${cors.enabled}")
		private boolean corsEnabled;
		@Value("${cors.allowed-origins}")
		private String corsAllowedOrigins;

		@Resource
		private FindByIndexNameSessionRepository<ExpiringSession> sessionRepository;

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable();  // 暂不支持CSRF

			// http.formLogin().loginPage("/toLogin");
			http.rememberMe().disable();

			if (corsEnabled) {
				CorsConfiguration config = new YcCorsConfiguration();
				config.setAllowCredentials(true);
				config.setAllowedOrigins(Arrays.asList(corsAllowedOrigins.split(",")));
				config.setAllowedMethods(Arrays.asList("*"));
				UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
				source.registerCorsConfiguration("/**", config);
				http.cors().configurationSource(source);
			}

			// // 需登录才可访问的接口
			// http.authorizeRequests().antMatchers("/user/current").authenticated();

//			http.authorizeRequests().antMatchers("/").permitAll();

		}
	}

	private static class YcCorsConfiguration extends CorsConfiguration {

		@Override
		public String checkOrigin(String requestOrigin) {
			if (!StringUtils.hasText(requestOrigin)) {
				return null;
			}
			for (String allowedOrigin : super.getAllowedOrigins()) {
				if (requestOrigin.endsWith(allowedOrigin)) {
					return requestOrigin;
				}
			}
			return super.checkOrigin(requestOrigin);
		}

	}

}
