package com.yc.portal.web.handle;

import java.util.HashMap;
import java.util.Map;

import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * 统一异常拦截。
 */
@ControllerAdvice
public class ExceptionHandle extends ResponseEntityExceptionHandler {

    private static final int DEFAULT_ERROR_CODE = ReturnCode.ERROR.getCode();

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public ResponseEntity<?> handle(Throwable ex) {
        int errorCode = DEFAULT_ERROR_CODE;
        String msg = null;
        if (ex instanceof ServiceException) {
            errorCode = ((ServiceException) ex).getCode();
            msg = ex.getMessage();
            logger.debug("ServiceException: ", ex);
        } else {
            logger.warn("Error: ", ex);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("errorCode", errorCode);
        result.put("msg", msg);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
