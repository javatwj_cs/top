package com.yc.portal.web.vo;

import java.util.ArrayList;
import java.util.List;

import com.yc.common.constant.BannerContentType;
import com.yc.core.model.Banner;

/**
 *
 */
public class BannerVo {

    private Long id;

    private String content;
    private String image;
    private BannerContentType contentType;  // 类型

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public BannerContentType getContentType() {
        return contentType;
    }

    public void setContentType(BannerContentType contentType) {
        this.contentType = contentType;
    }

    public static BannerVo convert(Banner banner) {
        if(banner == null) {
            return null;
        }

        BannerVo bannerVo = new BannerVo();

        bannerVo.setId(banner.getId());
        bannerVo.setContent(banner.getContent());
        bannerVo.setImage(banner.getImage());
        bannerVo.setContentType(banner.getContentType());

        return bannerVo;
    }

    public static List<BannerVo> converts(List<Banner> bannerList) {
        if(bannerList == null || bannerList.size() <= 0) {
            return null;
        }

        List<BannerVo> list = new ArrayList<>(bannerList.size());

        for(Banner banner : bannerList) {
            list.add(convert(banner));
        }


        return list;
    }

}
