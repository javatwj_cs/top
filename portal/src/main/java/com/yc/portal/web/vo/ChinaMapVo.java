package com.yc.portal.web.vo;

import com.yc.core.model.ChinaMap;

import java.util.ArrayList;
import java.util.List;


/**
 * 地址
 */
public class ChinaMapVo {

    private Long id;
    private String name;        // 名称

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ChinaMapVo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public static ChinaMapVo convert(ChinaMap chinaMap) {
        if(chinaMap == null) {
            return null;
        }

        ChinaMapVo chinaMapVo = new ChinaMapVo();

        chinaMapVo.setId(chinaMap.getId());
        chinaMapVo.setName(chinaMap.getName());

        return chinaMapVo;
    }

    public static List<ChinaMapVo> converts(List<ChinaMap> chinaMapList) {
        if(chinaMapList == null || chinaMapList.size() <= 0) {
            return null;
        }

        List<ChinaMapVo> list = new ArrayList<>(chinaMapList.size());

        for(ChinaMap chinaMap : chinaMapList) {
            list.add(convert(chinaMap));
        }


        return list;
    }
}
