package com.yc.portal.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yc.core.model.user.User;
import com.yc.core.service.user.UserService;
import com.yc.portal.web.vo.UserVo;

import io.swagger.annotations.ApiOperation;

/**
 * 系统配置项，杂项
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation("用户 - 注册 - 普通注册")
    @PostMapping("/register")
    public UserVo register(String username, String mobile, String mobileToken, String password, String recommendToken) {
        User user = userService.register(username, mobile, mobileToken, password, recommendToken);

        return UserVo.convert(user);
    }

    @ApiOperation("用户 - 登录 - 普通登录")
    @PostMapping("/login")
    public UserVo login(String username, String password) {
        User user = userService.login(username, password);

        return UserVo.convert(user);
    }

    @ApiOperation("用户 - 登录 - 手机登录")
    @PostMapping("/login/mobile")
    public UserVo loginMobile(String mobile, String password) {
        User user = userService.loginMobile(mobile, password);

        return UserVo.convert(user);
    }

    @ApiOperation("用户 - 登录 - 邮箱登录")
    @PostMapping("/login/email")
    public UserVo loginEmail(String email, String password) {
        User user = userService.loginEmail(email, password);

        return UserVo.convert(user);
    }

    @ApiOperation("用户 - 登录 - 微信登录")
    @PostMapping("/login/wx")
    public UserVo loginWx(String wxToken) {
        User user = userService.loginWx(wxToken);

        return UserVo.convert(user);
    }

    @ApiOperation("用户 - 微信 - 绑定")
    @PostMapping("/wx/bind")
    public void bindWx(String mobile, String mobileToken, String wxToken) {
        userService.bindWx(mobile, mobileToken, wxToken);
    }

    @ApiOperation("用户 - 微信 - 解除绑定")
    @PostMapping("/wx/remove")
    public void removeWx(String wxToken) {
        userService.removeWx(wxToken);
    }

    @ApiOperation("用户 - 修改密码")
    @PostMapping("/password")
    public void updatePasswordLogin(Long userId, String oldPassword, String newPassword) {
        userService.updatePasswordLogin(userId, oldPassword, newPassword);
    }
}
