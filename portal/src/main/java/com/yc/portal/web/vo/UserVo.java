package com.yc.portal.web.vo;

import com.yc.core.model.user.User;

/**
 *
 */
public class UserVo {

    private String username;        // 用户名
    private String mobile;          // 手机
    private String name;            // 真实姓名
    private String nickName;        // 昵称
    private String img;             // 头像

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile.substring(0,3) + "****" + mobile.substring(7);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public static UserVo convert(User user) {
        if(user == null) {
            return null;
        }

        UserVo userVo = new UserVo();

        userVo.setUsername(user.getUsername());
        userVo.setMobile(user.getMobile());
        userVo.setName(user.getName());
        userVo.setNickName(user.getNickName());
        userVo.setImg(user.getImg());

        return userVo;
    }

}
