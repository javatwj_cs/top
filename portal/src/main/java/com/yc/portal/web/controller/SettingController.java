package com.yc.portal.web.controller;

import com.yc.common.util.DateUtil;
import com.yc.core.model.ChinaMap;
import com.yc.core.service.ChinaMapService;
import com.yc.portal.web.vo.ChinaMapVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * 系统配置项，杂项
 */
@RestController
@RequestMapping("/setting")
public class SettingController {

    @Autowired
    private ChinaMapService chinaMapService;

    @ApiOperation("地址 - 省")
    @GetMapping("/map/province")
    public List<ChinaMapVo> getProvince() {
        List<ChinaMap> chinaMapList = chinaMapService.getProvince();

        return ChinaMapVo.converts(chinaMapList);
    }

    @ApiOperation("地址 - 市")
    @GetMapping("/map/city")
    public List<ChinaMapVo> getCity(Long id) {
        List<ChinaMap> chinaMapList = chinaMapService.getCity(id);

        return ChinaMapVo.converts(chinaMapList);
    }

    @ApiOperation("地址 - 区")
    @GetMapping("/map/area")
    public List<ChinaMapVo> getArea(Long id) {
        List<ChinaMap> chinaMapList = chinaMapService.getArea(id);

        return ChinaMapVo.converts(chinaMapList);
    }

    @ApiOperation("系统 - 当前时间")
    @GetMapping("/date")
    public Date getNowDate() {
        return DateUtil.now();
    }
}
