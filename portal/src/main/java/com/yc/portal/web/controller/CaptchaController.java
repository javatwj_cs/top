package com.yc.portal.web.controller;

import com.yc.common.util.RequestUtil;
import com.yc.core.cache.ImgToken;
import com.yc.core.cache.ImgTokenCache;
import com.yc.core.service.CaptchaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
@RequestMapping("/captcha")
public class CaptchaController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ImgTokenCache imgTokenCacheImpl;

    @Autowired
    private CaptchaService captchaService;

    @GetMapping("/img")
    public String getImgToken() {
        String imgKey = RequestUtil.getSessionId();

        ImgToken imgToken = imgTokenCacheImpl.set(imgKey);

        return imgToken.getToken();
    }

    @GetMapping("/mobile/register")
    public long getMobileRegisterToken(String mobile, String imgToken) {
        String imgKey = RequestUtil.getSessionId();

        return captchaService.sendRegisterToken(mobile, imgKey, imgToken);
    }

    @GetMapping("/mobile/wx")
    public long getMobileWxToken(String mobile, String imgToken) {
        String imgKey = RequestUtil.getSessionId();

        return captchaService.sendWxToken(mobile, imgKey, imgToken);
    }
}
