package com.yc.portal.web.controller;

import java.util.List;

import com.yc.common.constant.BannerPlace;
import com.yc.common.constant.Terminal;
import com.yc.common.util.RequestUtil;
import com.yc.core.model.Banner;
import com.yc.core.service.BannerService;
import com.yc.portal.web.vo.BannerVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yc.core.model.user.User;
import com.yc.core.service.user.UserService;
import com.yc.portal.web.vo.UserVo;

import io.swagger.annotations.ApiOperation;

/**
 * 系统配置项，杂项
 */
@RestController
@RequestMapping("/banner")
public class BannerController {

    @Autowired
    private BannerService bannerService;

    @ApiOperation("Banner - 展示")
    @GetMapping("/{place}")
    public List<BannerVo> getBanners(@PathVariable BannerPlace place) {
        List<Banner> bannerList = bannerService.getList(place, RequestUtil.determineTerminal());

        return BannerVo.converts(bannerList);
    }
}
