package com.yc.core;

import com.yc.common.util.KryoFactory;
import com.yc.common.util.KryoSerializer;
import com.yc.core.repository.sql.EntityManagerRepositoryImpl;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.cache.CacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

/**
 * Spring共同配置，所有依赖当前工程的项目，必须将当前类引入Spring配置中。
 *
 */
@Configuration
@ComponentScan
@EnableCaching(proxyTargetClass = true)
@EnableAutoConfiguration
@ImportResource("classpath:applicationContext-tx.xml")
@PropertySource("classpath:application-core.properties")
@EnableJpaRepositories(repositoryBaseClass = EntityManagerRepositoryImpl.class)
public class AppConfig {

    @Resource
    private Environment env;

    @Bean
    public CacheManagerCustomizer<RedisCacheManager> cacheManagerCustomizer() {
        return (CacheManagerCustomizer<RedisCacheManager>) cacheManager -> {
            cacheManager.setTransactionAware(true);
            cacheManager.setLoadRemoteCachesOnStartup(false);
        };
    }

    /**
     * 覆盖Spring默认MongoDB类型关联，以去除内联类型中的“_class”字段。
     *
     * @param mongoDbFactory MongoDbFactory
     * @param mongoMappingContext MongoMappingContext
     * @return 实例
     * @see org.springframework.data.mongodb.config.AbstractMongoConfiguration#mappingMongoConverter()
     */
    @Bean
    public MappingMongoConverter mappingMongoConverter(MongoDbFactory mongoDbFactory, MongoMappingContext mongoMappingContext) {
        DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory);
        MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
        converter.setTypeMapper(new DefaultMongoTypeMapper(null)); // remove "_class" in mongo document
        return converter;
    }

    @Bean
    public RestTemplate restTemplate() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        // SSLContextBuilder sslContextBuilder = new SSLContextBuilder().loadTrustMaterial(null, (chain, authType) ->
        // true);
        // RequestConfig.Builder configBuilder = RequestConfig.custom();
        // configBuilder.setConnectTimeout(5000);
        // configBuilder.setConnectionRequestTimeout(5000);
        // HttpClientBuilder builder = HttpClientBuilder.create();
        // builder.setSSLContext(sslContextBuilder.build());
        // builder.setDefaultRequestConfig(configBuilder.build());
        // ClientHttpRequestFactory f = new HttpComponentsClientHttpRequestFactory(builder.build());
        SimpleClientHttpRequestFactory f = new SimpleClientHttpRequestFactory();
        f.setConnectTimeout(5000);
        f.setReadTimeout(5000);
        return new RestTemplate(f);
    }

    /**
     * 用于访问接入的第三方游戏平台HTTP接口，允许在配置文件中设置代理。
     *
     * @return 实例
     */
    @Bean
    public RestTemplate proxyRestTemplate() {
        Integer timeout = env.getRequiredProperty("rest_template.http.timeout", Integer.class);
        String proxyType = env.getProperty("rest_template.proxy.type");
        SimpleClientHttpRequestFactory f = new SimpleClientHttpRequestFactory();
        f.setConnectTimeout(timeout);
        f.setReadTimeout(timeout);
        if (proxyType != null && proxyType.length() > 0) {
            Type type = Type.valueOf(proxyType);
            String proxyHost = env.getRequiredProperty("rest_template.proxy.host");
            Integer proxyPort = env.getRequiredProperty("rest_template.proxy.port", Integer.class);
            f.setProxy(new Proxy(type, new InetSocketAddress(proxyHost, proxyPort)));
        }
        return new RestTemplate(f);
    }

    /**
     * 正、反序列化工具类。
     *
     * @return 实例
     */
    @Bean
    public KryoSerializer kryoSerializer() {
        return new KryoSerializer(new KryoFactory());
    }
}
