package com.yc.core.service.message.sms;

import com.yc.core.model.message.SmsChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class SimpleSms implements SmsSenderContent {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void send(SmsChannel smsChannel, String mobile, String content) {

        logger.info("--> SimpleSms.send{{}:{}}", mobile, content);
    }
}
