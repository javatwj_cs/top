package com.yc.core.service.message.email;

import com.yc.common.util.Verification;
import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.core.model.admin.AdminUser;
import com.yc.core.model.message.EmailChannel;
import com.yc.core.model.message.EmailLog;
import com.yc.core.repository.sql.message.EmailChannelRepository;
import com.yc.core.repository.sql.message.EmailLogRepository;
import com.yc.core.service.message.IMessageChannelService;
import com.yc.core.service.message.MessageAction;
import com.yc.core.service.message.Recipient;
import com.yc.core.service.message.SendStatus;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 邮件发送。任何发送渠道，禁止抛错！！即使出错，保留错误信息
 */
@Service("emailService")
public class EmailService implements IMessageChannelService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EmailLogRepository emailLogRepository;
    @Autowired
    private EmailChannelRepository emailChannelRepository;

    private static ConcurrentHashMap<Long, JavaMailSenderImpl> mailSenders = new ConcurrentHashMap<>();

    /**
     * 邮箱发送参数
     * @return
     */
    private Properties getMailProperties() {
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.debug", "false");
        return properties;
    }

    /**
     * 获取mail发送示例，享元模式
     * @param emailChannel
     * @return
     */
    private JavaMailSenderImpl getJavaMailSenderImpl(EmailChannel emailChannel) {
        JavaMailSenderImpl javaMailSender = mailSenders.get(emailChannel.getId());

        // 如果emailChannel有更改，需要重新生成javaMailSender
        if (javaMailSender != null && javaMailSender.getProtocol().equals(emailChannel.getProtocol()) && javaMailSender.getHost().equals(emailChannel.getHost()) && javaMailSender.getUsername()
                .equals(emailChannel.getUsername()) && javaMailSender.getPassword().equals(emailChannel.getPassword())) {

            return javaMailSender;
        }

        javaMailSender = new JavaMailSenderImpl();

        javaMailSender.setProtocol(emailChannel.getProtocol());
        javaMailSender.setHost(emailChannel.getHost());
        javaMailSender.setUsername(emailChannel.getUsername());
        javaMailSender.setPassword(emailChannel.getPassword());
        javaMailSender.setJavaMailProperties(getMailProperties());

        mailSenders.put(emailChannel.getId(), javaMailSender);

        return javaMailSender;
    }

    /**
     * 获取邮箱发送渠道
     * @param email
     * @param messageAction
     * @return
     */
    private EmailChannel getChannel(String email, MessageAction messageAction) {
        // TODO 渠道算法
        // TODO 检查渠道是否重复（策略算法）
        // TODO 邮箱渠道要加入缓存

        List<EmailChannel> emailChannelList = emailChannelRepository.findAll();

        if (emailChannelList == null || emailChannelList.size() <= 0) {
            throw new ServiceException(ReturnCode.CODE_50102, "渠道不存在");
        }

        return emailChannelList.get(RandomUtils.nextInt(0, emailChannelList.size()));
    }

    /**
     * 发送邮箱
     * @param emailChannel
     * @param email
     * @param content
     * @return
     */
    private void sendMail(EmailChannel emailChannel, String email, String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom(emailChannel.getUsername());
        message.setTo(email);
        message.setSubject(subject);
        message.setText(content);

        JavaMailSenderImpl javaMailSender = getJavaMailSenderImpl(emailChannel);
        javaMailSender.send(message);
    }

    @Override
    public void send(MessageAction messageAction, Recipient recipient, String content, AdminUser sender) {
        EmailLog emailLog = new EmailLog();

        emailLog.setUser(recipient.getUser());
        emailLog.setEmail(recipient.getEmail());
        emailLog.setSubject(messageAction.getSubject());
        emailLog.setContent(content);

        emailLog.setSender(sender);
        emailLog.setSenderName(sender == null ? null : sender.getUsername());
        emailLog.setMessageAction(messageAction);

        // 发送邮件
        EmailChannel emailChannel = null;
        SendStatus status;
        try {
            // 如果邮箱格式不正确，只记录不发送
            if (!Verification.isValidEmail(recipient.getEmail())) {
                throw new ServiceException(ReturnCode.CODE_50102, "格式不合法");
            }

            emailChannel = getChannel(recipient.getEmail(), messageAction);                      // 获取邮箱渠道
            sendMail(emailChannel, recipient.getEmail(), messageAction.getSubject(), content);  // 发送邮箱

            status = SendStatus.SUCCESS;

        } catch (Exception e) {
            logger.error("发送邮箱失败：{}", e);
            status = SendStatus.FAILURE;

            emailLog.setReturnMessage(e.getMessage());
        }

        emailLog.setChannel(emailChannel);
        emailLog.setSendStatus(status);

        emailLogRepository.save(emailLog);
    }

    @Override
    public void send(MessageAction messageAction, Recipient recipient, String content) {
        send(messageAction, recipient, content, null);
    }
}
