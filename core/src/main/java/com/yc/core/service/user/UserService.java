package com.yc.core.service.user;

import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.common.util.RequestUtil;
import com.yc.common.util.Verification;
import com.yc.core.cache.ImgTokenCache;
import com.yc.core.cache.MobileToken;
import com.yc.core.cache.MobileTokenCache;
import com.yc.core.model.user.User;
import com.yc.core.repository.sql.user.UserRepository;
import com.yc.core.service.account.AccountService;
import com.yc.core.service.message.MessageActionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;

/**
 * Created by tianjie on 17/7/17.
 */
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private MobileTokenCache mobileTokenCache;


    public void registerSuccess(User user, String recommendToken) {
        // TODO 需根据实际需求完善

        // 初始化账户
        accountService.add(user);
    }

    public User register(String username, String mobile, String mobileToken, String password, String recommendToken) {
        if(!Verification.isValidUsername(username)) {
            throw new ServiceException(ReturnCode.CODE_40004, "用户名无效");
        }
        if(!Verification.isValidPassword(password)) {
            throw new ServiceException(ReturnCode.CODE_40004, "密码");
        }
        if(!Verification.isValidMobile(mobile)) {
            throw new ServiceException(ReturnCode.CODE_40004, "手机号码");
        }
        if(!mobileTokenCache.checkToken(mobile, mobileToken)) {
            throw new ServiceException(ReturnCode.CODE_40004, "手机验证码");
        }

        User user = this.getUserByUsername(username);
        if(user != null && user.getId() > 0) {
            throw new ServiceException(ReturnCode.CODE_40006, "用户名");
        }
        user = this.getUserByMobile(mobile);
        if(user != null && user.getId() > 0) {
            throw new ServiceException(ReturnCode.CODE_40006, "手机号码");
        }

        user = new User();

        user.setUsername(username);
        user.setMobile(mobile);
        user.setPasswordLogin(password);
        user.setNickName(username);

        user = userRepository.save(user);

        this.registerSuccess(user, recommendToken);


        return user;
    }

    public void loginSuccess(User user) {
        user.setLoginLastIp(RequestUtil.getAddress());
        user.setLoginLastTime(new Date());

        // TODO 需根据实际需求完善

        // 初始化账户
    }

    public User login(String username, String password) {
        if(!Verification.isValidUsername(username)) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户名或密码错误");
        }
        if(!Verification.isValidPassword(password)) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户名或密码错误");
        }

        User user = this.getUserByUsername(username);
        if(user == null || user.getId() == null || user.getId() <= 0) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户不存在");
        }
        if(!Objects.equals(password, user.getPasswordLogin())) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户名或密码错误");
        }

        this.loginSuccess(user);

        return user;
    }

    public User loginMobile(String mobile, String password) {
        if(!Verification.isValidPassword(password)) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户名或密码错误");
        }
        if(!Verification.isValidMobile(mobile)) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户名或密码错误");
        }

        User user = this.getUserByMobile(mobile);
        if(user == null || user.getId() == null || user.getId() <= 0) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户不存在");
        }
        if(!Objects.equals(password, user.getPasswordLogin())) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户名或密码错误");
        }


        this.loginSuccess(user);


        return user;
    }

    public User loginEmail(String email, String password) {
        if(!Verification.isValidPassword(password)) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户名或密码错误");
        }
        if(!Verification.isValidEmail(email)) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户名或密码错误");
        }

        User user = this.getUserByEmail(email);
        if(user == null || user.getId() == null || user.getId() <= 0) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户不存在");
        }
        if(!Objects.equals(password, user.getPasswordLogin())) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户名或密码错误");
        }


        this.loginSuccess(user);


        return user;
    }

    public User loginWx(String wxToken) {
        if(StringUtils.isBlank(wxToken)) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户不存在");
        }

        User user = this.getUserByWxToken(wxToken);
        if(user == null || user.getId() == null || user.getId() <= 0) {
            throw new ServiceException(ReturnCode.CODE_50201, "用户不存在");
        }


        this.loginSuccess(user);

        return user;
    }

    public void updatePasswordLogin(Long userId, String oldPassword, String newPassword) {
        if(userId == null || userId <= 0) {
            throw new ServiceException(ReturnCode.CODE_40004, "用户");
        }

        if(!Verification.isValidPassword(oldPassword) || !Verification.isValidPassword(newPassword)) {
            throw new ServiceException(ReturnCode.CODE_40004, "密码");
        }

        User user = this.getUser(userId);
        if(user == null) {
            throw new ServiceException(ReturnCode.CODE_40005, "用户");
        }
        if(!Objects.equals(oldPassword, user.getPasswordLogin())) {
            throw new ServiceException(ReturnCode.CODE_50202, "旧密码错误");
        }

        user.setPasswordLogin(newPassword);
    }

    public void bindWx(String mobile, String mobileToken, String wxToken) {
        if(!Verification.isValidMobile(mobile)) {
            throw new ServiceException(ReturnCode.CODE_40004, "手机号码");
        }
        if(!mobileTokenCache.checkToken(mobile, mobileToken)) {
            throw new ServiceException(ReturnCode.CODE_40004, "手机验证码");
        }
        if(StringUtils.isBlank(wxToken)) {
            throw new ServiceException(ReturnCode.CODE_40004, "微信");
        }

        User user = this.getUserByMobile(mobile);
        if(user == null) {
            throw new ServiceException(ReturnCode.CODE_40004, "用户");
        }

        user.setWxToken(wxToken);
    }

    public void removeWx(String wxToken) {
        if(StringUtils.isBlank(wxToken)) {
            throw new ServiceException(ReturnCode.CODE_40004, "微信");
        }

        User user = this.getUserByWxToken(wxToken);
        if(user == null) {
            throw new ServiceException(ReturnCode.CODE_40005, "用户");
        }

        user.setWxToken(null);
    }

    public User getUser(Long id) {
        return userRepository.findOne(id);
    }

    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User getUserByMobile(String mobile) {
        return userRepository.findByMobile(mobile);
    }


    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }


    public User getUserByWxToken(String wxToken) {
        return userRepository.findByWxToken(wxToken);
    }
}
