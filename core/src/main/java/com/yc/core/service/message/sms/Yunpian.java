package com.yc.core.service.message.sms;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.core.model.message.SmsChannel;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.io.IOException;

@Component
public class Yunpian implements SmsSenderContent {

    private static final String SEND_URL = "SEND_URL";
    private static final String APP_KEY = "APP_KEY";

    @Resource
    private RestTemplate restTemplate;
    @Resource
    private ObjectMapper objectMapper;

    @Override
    public void send(SmsChannel smsChannel, String mobile, String content) {
        send(mobile, content);
    }

    /**
     * 注意：云片短信内容必须与审核内容完全一致才可以正常发送。
     *
     * @param mobile 手机号
     * @param content 短信内容
     */
    public void send(String mobile, String content) {

        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("apikey", APP_KEY);
        body.add("mobile", mobile);
        body.add("text", content);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        ResponseEntity<byte[]> response = restTemplate.exchange(SEND_URL, HttpMethod.POST, new HttpEntity<>(body, headers), byte[].class);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new ServiceException(ReturnCode.CODE_50101, "发送失败");
        }
        // {"code":0,"msg":"发送成功","count":1,"fee":0.05,"unit":"RMB","mobile":"13552329360","sid":16494300528}

        JsonNode result;
        try {
            result = objectMapper.readTree(response.getBody());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        JsonNode code = result.get("code");
        if (code == null || code.asInt() != 0) {
            throw new RuntimeException("云片短信发送失败：" + result);
        }

    }

}
