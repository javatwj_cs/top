package com.yc.core.service;

import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.core.model.ChinaMap;
import com.yc.core.repository.sql.ChinaMapRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ChinaMapService {

    @Autowired
    private ChinaMapRepository chinaMapRepository;

    private List<ChinaMap> getChinaMap(String provinceId, String cityId) {

        return chinaMapRepository.findAll(provinceId, cityId);
    }

    /**
     * 检查地址是否包含指定类型
     * type = 1 : 省
     * type = 2 : 市
     * type = 3 : 区
     * @param id
     * @param type
     * @return
     */
    private boolean contain(Long id, int type) {
        ChinaMap chinaMap = chinaMapRepository.findOne(id);
        if(chinaMap == null) {
            return false;
        }

        switch (type) {
            case 1:
                return true;
            case 2:
                if(StringUtils.isNotBlank(chinaMap.getProvinceId())) {
                    return true;
                }
                return false;
            case 3:
                if(StringUtils.isNotBlank(chinaMap.getProvinceId()) && StringUtils.isNotBlank(chinaMap.getCityId())) {
                    return true;
                }
                return false;

            default:
                return false;
        }
    }

    public List<ChinaMap> getProvince() {
        return getChinaMap(null, null);
    }

    public List<ChinaMap> getCity(Long id) {
        if(id == null || id <= 0) {
            return null;
        }

        ChinaMap chinaMap = chinaMapRepository.findOne(id);
        if(chinaMap == null || StringUtils.isBlank(chinaMap.getCode())) {
            return null;
        }

        return chinaMapRepository.findAll(chinaMap.getCode(), null);
    }

    public List<ChinaMap> getArea(Long id) {
        if(id == null || id <= 0) {
            return null;
        }

        ChinaMap chinaMap = chinaMapRepository.findOne(id);
        if(chinaMap == null || StringUtils.isBlank(chinaMap.getProvinceId()) || StringUtils.isBlank(chinaMap.getCode())) {
            return null;
        }

        return chinaMapRepository.findAll(chinaMap.getProvinceId(), chinaMap.getCode());
    }

    public String getAddress(Long id){
        // province_id（空） city_id（空）     省
        // province_id（非空） city_id（空）   市
        // province_id（非空） city_id（非空） 区/县

        ChinaMap chinaMap;

        ChinaMap province = null;
        ChinaMap city = null;
        ChinaMap area = null;

        chinaMap = chinaMapRepository.findOne(id);
        if(chinaMap == null) {
            return null;
        }

        if(StringUtils.isNotBlank(chinaMap.getProvinceId()) && StringUtils.isNotBlank(chinaMap.getCityId())) {
            province = chinaMapRepository.findOne(chinaMap.getProvinceId(), null, null);
            city = chinaMapRepository.findOne(chinaMap.getCityId(), chinaMap.getProvinceId(), null);
            area = chinaMap;
        }
        if(StringUtils.isNotBlank(chinaMap.getProvinceId()) && StringUtils.isBlank(chinaMap.getCityId())) {
            province = chinaMapRepository.findOne(chinaMap.getProvinceId(), null, null);
            city = chinaMap;
        }
        if(StringUtils.isBlank(chinaMap.getProvinceId()) && StringUtils.isBlank(chinaMap.getCityId())) {
            province = chinaMap;
        }


        StringBuilder sb = new StringBuilder();

        sb.append(province.getName() + (province.getName().endsWith("省") ? "" : "省"));

        if(city != null) {
            sb.append(city.getName() + (city.getName().endsWith("市") ? "" : "市"));
        }
        if(area != null) {
            sb.append(area.getName());
        }

        return sb.toString();
    }


    public boolean containProvince(Long id) {
        return contain(id,1);
    }
    public boolean containCity(Long id) {
        return contain(id,2);
    }
    public boolean containArea(Long id) {
        return contain(id,3);
    }

    public boolean isEnd(Long id) {
        if(id == null || id <= 0) {
            throw new ServiceException(ReturnCode.CODE_40004, "地址");
        }

        ChinaMap chinaMap = chinaMapRepository.findOne(id);
        if(chinaMap == null) {
            throw new ServiceException(ReturnCode.CODE_40004, "地址");
        }

        if(StringUtils.isNotBlank(chinaMap.getProvinceId()) && StringUtils.isNotBlank(chinaMap.getCityId())) {
            return true;
        }
        if(StringUtils.isNotBlank(chinaMap.getProvinceId()) && StringUtils.isBlank(chinaMap.getCityId())) {
            List<ChinaMap> list = chinaMapRepository.findAll(chinaMap.getProvinceId(), chinaMap.getCode());

            return list == null || list.size() <= 0;
        }
        if(StringUtils.isBlank(chinaMap.getProvinceId()) && StringUtils.isBlank(chinaMap.getCityId())) {
            List<ChinaMap> list = chinaMapRepository.findAll(chinaMap.getCode(), null);

            return list == null || list.size() <= 0;
        }

        return false;
    }

}