package com.yc.core.service.message;

import com.yc.core.model.admin.AdminUser;

/**
 * 发送消息渠道
 */
public interface IMessageChannelService {
    public void send(MessageAction messageAction, Recipient recipient, String content);
    public void send(MessageAction messageAction, Recipient recipient, String content, AdminUser sender);
}
