package com.yc.core.service.message.sms;


import com.yc.core.model.message.SmsChannel;

/**
 * 短信发送 - 内容
 */
public interface SmsSenderContent extends SmsSender {
    void send(SmsChannel smsChannel, String mobile, String content);
}
