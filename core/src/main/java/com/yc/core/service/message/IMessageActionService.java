package com.yc.core.service.message;


import com.yc.core.model.admin.AdminUser;
import com.yc.core.model.user.User;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 发送消息
 */
public interface IMessageActionService {

    /**
     * 手机注册
     * @param mobile
     * @param mobileToken
     */
    public void sendRegisterMobile(String mobile, String mobileToken);

    /**
     * 绑定微信
     * @param user
     * @param mobileToken
     */
    public void sendBindWx(User user, String mobileToken);
//
//    /**
//     * 修改手机号
//     * @param mobile
//     * @param mobileToken
//     */
//    public void sendUpdateMobile(User user, String mobile, String mobileToken);
//
//    /**
//     * 注册成功
//     * @param user
//     */
//    public void sendRegisterSuccess(User user);
//
//    /**
//     * 存款/充值成功（线下）
//     * @param user
//     * @param depositOfflineAmount
//     * @param balance
//     */
//    public void sendDepositOfflineSuccess(User user, BigDecimal depositOfflineAmount, BigDecimal balance);
//
//    /**
//     * 提现申请
//     * @param user
//     * @param withdrawApplyDate
//     * @param withdrawAmount
//     */
//    public void sendWithdrawApply(User user, Date withdrawApplyDate, BigDecimal withdrawAmount);
//
//    /**
//     * 提现成功
//     * @param user
//     * @param withdrawApplyDate
//     * @param withdrawAmount
//     */
//    public void sendWithdrawSuccess(User user, Date withdrawApplyDate, BigDecimal withdrawAmount);
//
//    /**
//     * 优惠到账/成功
//     * @param user
//     * @param favorableApplyDate
//     * @param favorableName
//     */
//    public void sendFavorableSuccess(User user, Date favorableApplyDate, String favorableName);
//
//    /**
//     * 洗码到账
//     * @param user
//     * @param rebateAmount
//     */
//    public void sendRebateSuccess(User user, BigDecimal rebateAmount);
//
//    /**
//     * 佣金到账
//     * @param user
//     * @param commissionCycle
//     * @param commissionAmount
//     */
//    public void sendCommissionSuccess(User user, String commissionCycle, BigDecimal commissionAmount);
//
//    /**
//     * 手机找回密码
//     * @param user
//     * @param mobileToken
//     */
//    public void sendFindPasswordMobile(User user, String mobileToken);
//
//    /**
//     * 邮箱找回密码
//     * @param user
//     * @param emailToken
//     */
//    public void sendFindPasswordEmail(User user, String emailToken);
//
//    /**
//     * 绑定邮箱
//     * @param email
//     * @param emailActivatingUrl
//     */
//    public void sendBindEmail(User user, String email, String emailActivatingUrl);
//
//    /**
//     * 留言回复
//     * @param user
//     * @param questionDate
//     */
//    public void sendQuestionReply(User user, Date questionDate);
//
//    /**
//     * 代理申请成功
//     * @param user
//     */
//    public void sendAgentApplySuccess(User user);
//
//    /**
//     * 用户会员等级变动
//     * @param user
//     * @param vipName
//     */
//    public void sendUserVipUpdate(User user, String vipName);
//
//    /**
//     * 手机找回账户密码
//     * @param user
//     * @param mobileToken
//     */
//    public void sendFindAccountPasswordMobile(User user, String mobileToken);
//
//    /**
//     * 邮箱找回账户密码
//     * @param user
//     * @param emailToken
//     */
//    public void sendFindAccountPasswordEmail(User user, String emailToken);
//
//    /**
//     * 管理员发送自定义内容
//     * @param user
//     * @param content
//     * @param sender
//     */
//    public void sendUserServiceEmail(AdminUser sender, User user, String content);
//
//    /**
//     * 管理员发送自定义内容
//     * @param sender
//     * @param user
//     * @param content
//     */
//    public void sendUserServiceMobile(AdminUser sender, User user, String content);
//
//    /**
//     * 管理员发送自定义内容
//     * @param sender
//     * @param user
//     * @param content
//     */
//    public void sendUserServiceStationLetter(AdminUser sender, User user, String content);
}
