package com.yc.core.service.account;

import com.yc.common.constant.AccountType;
import com.yc.common.constant.BillType;
import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.core.model.account.Account;
import com.yc.core.model.account.AccountBill;
import com.yc.core.model.user.User;
import com.yc.core.repository.sql.account.AccountBillRepository;
import com.yc.core.repository.sql.account.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Created by tianjie on 17/8/20.
 */
public abstract class AbstractAccountService {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private AccountBillRepository accountBillRepository;

    public Account add(User user) {
        Account entity = new Account();

        entity.setUser(user);
        entity.setAmount(BigDecimal.ZERO);
        entity.setFreezeAmount(BigDecimal.ZERO);
        entity.setIntegral(0);

        entity.setSign(entity.generateSign());


        return accountRepository.save(entity);
    }

    public AccountBill increment(User user, AccountType accountType, BillType billType, BigDecimal amount) {
        Account account = user.getAccount();
        BigDecimal amountBefore = null;
        BigDecimal amountAfter = null;

        // 检验签名
        if(!Objects.equals(account.generateSign(), account.getSign())) {
            throw new ServiceException(ReturnCode.CODE_50300, "请联系客服");
        }

        if(accountType == AccountType.AMOUNT) {
            amountBefore = account.getAmount();
            account.setAmount(account.getAmount().add(amount));
            amountAfter = account.getAmount();
        }else if(accountType == AccountType.FREEZE_AMOUNT) {
            amountBefore = account.getFreezeAmount();
            account.setAmount(account.getAmount().add(amount));
            amountAfter = account.getFreezeAmount();
        }else if(accountType == AccountType.INTEGRAL) {
            amountBefore = new BigDecimal("" + account.getIntegral());
            account.setAmount(account.getAmount().add(amount));
            amountAfter = new BigDecimal("" + account.getIntegral());
        }

        // 数据签名
        account.setSign(account.generateSign());


        AccountBill entity = new AccountBill();

        entity.setUser(user);
        entity.setAccountType(accountType);
        entity.setBillType(billType);
        entity.setAmount(amount);
        entity.setAmountBefore(amountBefore);
        entity.setAmountAfter(amountAfter);

        return accountBillRepository.save(entity);
    }

    public AccountBill reduction(User user, AccountType accountType, BillType billType, BigDecimal amount) {
        Account account = user.getAccount();
        BigDecimal amountBefore = null;
        BigDecimal amountAfter = null;

        // 检验签名
        if(!Objects.equals(account.generateSign(), account.getSign())) {
            throw new ServiceException(ReturnCode.CODE_50300, "请联系客服");
        }

        if(accountType == AccountType.AMOUNT) {
            if (account.getAmount().compareTo(amount) <= 0) {
                throw new ServiceException(ReturnCode.CODE_50301, "资金不足");
            }

            amountBefore = account.getAmount();
            account.setAmount(account.getAmount().subtract(amount));
            amountAfter = account.getAmount();
        }else if(accountType == AccountType.FREEZE_AMOUNT) {
            if (account.getFreezeAmount().compareTo(amount) <= 0) {
                throw new ServiceException(ReturnCode.CODE_50302, "资金不足");
            }

            amountBefore = account.getFreezeAmount();
            account.setAmount(account.getAmount().subtract(amount));
            amountAfter = account.getFreezeAmount();
        }else if(accountType == AccountType.INTEGRAL) {
            if (account.getIntegral().compareTo(amount.intValue()) <= 0) {
                throw new ServiceException(ReturnCode.CODE_50303, "积分不足");
            }

            amountBefore = new BigDecimal("" + account.getIntegral());
            account.setAmount(account.getAmount().subtract(amount));
            amountAfter = new BigDecimal("" + account.getIntegral());
        }

        // 数据签名
        account.setSign(account.generateSign());


        AccountBill entity = new AccountBill();

        entity.setUser(user);
        entity.setAccountType(accountType);
        entity.setBillType(billType);
        entity.setAmount(amount);
        entity.setAmountBefore(amountBefore);
        entity.setAmountAfter(amountAfter);

        return accountBillRepository.save(entity);
    }
}
