package com.yc.core.service.message;

/**
 * 消息渠道
 */
public enum MessageChannel {

    STATION_LETTER, // 站内信
    SMS,            // 短信
    EMAIL           // 邮箱

    ;
}
