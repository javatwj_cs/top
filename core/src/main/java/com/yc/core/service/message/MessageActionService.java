package com.yc.core.service.message;

import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.core.model.admin.AdminUser;
import com.yc.core.model.message.MessageParamManage;
import com.yc.core.model.message.UserMessageClose;
import com.yc.core.model.user.User;
import com.yc.core.repository.sql.message.MessageParamManageRepository;
import com.yc.core.repository.sql.message.UserMessageCloseRepository;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.logging.log4j.core.util.StringBuilderWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.AbstractMap.SimpleEntry;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 消息动作
 */
@Service
public class MessageActionService implements IMessageActionService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IMessageChannelService stationLetterService;
    @Autowired
    private IMessageChannelService emailService;
    @Autowired
    private IMessageSmsChannelService smsService;

    @Autowired
    private MessageParamManageRepository messageParamManageRepository;
    @Autowired
    private UserMessageCloseRepository userMessageCloseRepository;

    @Autowired
    private freemarker.template.Configuration configuration;

    private final Map<Long, Entry<Integer, Template>> cache = new ConcurrentHashMap<>();

    public String render(MessageParamManage message, Object params) {
        Entry<Integer, Template> entry = cache.get(message.getId());

        // 缓存不存在或者版本号变更，重建模板
        if (entry == null || !entry.getKey().equals(message.getVersion())) {
            Template template;
            try {
                template = new Template(message.getId().toString(), message.getContent(), configuration);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            entry = new SimpleEntry<>(message.getVersion(), template);
            cache.put(message.getId(), entry);
        }

        StringBuilderWriter writer = new StringBuilderWriter(message.getContent().length());
        try {
            entry.getValue().process(params, writer);
        } catch (TemplateException | IOException e) {
            throw new RuntimeException(e);
        }
        return writer.toString();
    }

    /**
     * 获取消息参数管理。通过平台和用户，组合获取最终结果
     *
     * @param user
     * @param messageAction
     * @return
     */
    private List<MessageParamManage> getMessageParamManages(User user, MessageAction messageAction) {
        // TODO 消息参数管理需要加入到缓存
        // 检测此消息动作，平台发送形式
        List<MessageParamManage> messageParamManages = messageParamManageRepository.findByMessageAction(messageAction);
        if (messageParamManages == null || messageParamManages.size() <= 0) {
            return messageParamManages;
        }

        if (user == null || user.getId() == null || user.getId() <= 0) {
            return messageParamManages;
        }

        // 检测此消息动作，用户是否有设置接收
        List<UserMessageClose> userMessageCloses = userMessageCloseRepository.findByUserId(user.getId());
        // 通过用户设置，过滤 messageParamManages
        for (UserMessageClose userMessageClose : userMessageCloses) {
            messageParamManages.remove(userMessageClose.getMessageParamManage());
        }

        return messageParamManages;
    }

    /**
     * 消息发送路由
     *
     * @param messageChannel
     * @param recipient
     * @param content
     * @param messageAction
     * @param messageParamDynamicVo
     */
    private void sendRouting(MessageChannel messageChannel, MessageAction messageAction, Recipient recipient, String content, MessageParamDynamicVo messageParamDynamicVo) {
        if (messageChannel == MessageChannel.STATION_LETTER) {
            stationLetterService.send(messageAction, recipient, content);
        } else if (messageChannel == MessageChannel.EMAIL) {
            emailService.send(messageAction, recipient, content);
        } else if (messageChannel == MessageChannel.SMS) {
            smsService.send(messageAction, recipient, content, messageParamDynamicVo);
        } else {
            logger.error("MessageAction.sendRouting error. messageChannel:{}", messageChannel.name());

            throw new ServiceException(ReturnCode.CODE_50100, "消息渠道不合法");
        }
    }

    /**
     * 发送消息
     *
     * @param messageAction
     * @param recipient
     * @param messageParamDynamicVo
     */
    private void sendMessage(MessageAction messageAction, Recipient recipient, MessageParamDynamicVo messageParamDynamicVo) {
        // 获取发送消息的参数信息
        List<MessageParamManage> messageParamManages = getMessageParamManages(recipient.getUser(), messageAction);
        if (messageParamManages == null || messageParamManages.size() <= 0) {
            return;
        }

        // 循环发送消息的参数信息，进行发送消息
        String content;
        for (MessageParamManage messageParamManage : messageParamManages) {
            // 过滤文案中消息动态内容
            // content = messageParamDynamicVo.filter(messageParamManage.getContent());
            content = render(messageParamManage, messageParamDynamicVo);
            // 进行发送
            sendRouting(messageParamManage.getMessageChannel(), messageAction, recipient, content, messageParamDynamicVo);
        }
    }

    @Override
    public void sendRegisterMobile(String mobile, String mobileToken) {
        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
        messageParamDynamicVo.setMobileToken(mobileToken);

        Recipient recipient = new Recipient();
        recipient.setMobile(mobile);

        sendMessage(MessageAction.REGISTER_MOBILE, recipient, messageParamDynamicVo);
    }

    @Override
    public void sendBindWx(User user, String mobileToken) {
        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
        messageParamDynamicVo.setMobileToken(mobileToken);

        Recipient recipient = Recipient.convert(user);

        sendMessage(MessageAction.BIND_WX, recipient, messageParamDynamicVo);
    }

//
//    @Override
//    public void sendUpdateMobile(User user, String mobile, String mobileToken) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setMobileToken(mobileToken);
//
//        Recipient recipient = Recipient.convert(user);
//        recipient.setMobile(mobile);
//
//        sendMessage(MessageAction.UPDATE_MOBILE, recipient, messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendRegisterSuccess(User user) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setUsername(user.getUsername());
//
//        sendMessage(MessageAction.REGISTER_SUCCESS, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendDepositOfflineSuccess(User user, BigDecimal depositOfflineAmount, BigDecimal balance) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setDepositAmount(depositOfflineAmount);
//        messageParamDynamicVo.setBalance(balance);
//
//        sendMessage(MessageAction.DEPOSIT_OFFLINE_SUCCESS, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendWithdrawApply(User user, Date withdrawApplyDate, BigDecimal withdrawAmount) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setWithdrawApplyDate(withdrawApplyDate);
//        messageParamDynamicVo.setWithdrawAmount(withdrawAmount);
//        messageParamDynamicVo.setUsername(user.getUsername());
//
//        sendMessage(MessageAction.WITHDRAW_APPLY, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendWithdrawSuccess(User user, Date withdrawApplyDate, BigDecimal withdrawAmount) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setWithdrawApplyDate(withdrawApplyDate);
//        messageParamDynamicVo.setWithdrawAmount(withdrawAmount);
//        messageParamDynamicVo.setUsername(user.getUsername());
//
//        sendMessage(MessageAction.WITHDRAW_SUCCESS, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendFavorableSuccess(User user, Date favorableApplyDate, String favorableName) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setUsername(user.getUsername());
//        messageParamDynamicVo.setFavorableApplyDate(favorableApplyDate);
//        messageParamDynamicVo.setFavorableName(favorableName);
//
//        sendMessage(MessageAction.FAVORABLE_SUCCESS, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendRebateSuccess(User user, BigDecimal rebateAmount) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setUsername(user.getUsername());
//        messageParamDynamicVo.setRebateAmount(rebateAmount);
//
//        sendMessage(MessageAction.REBATE_SUCCESS, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendCommissionSuccess(User user, String commissionCycle, BigDecimal commissionAmount) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setUsername(user.getUsername());
//        messageParamDynamicVo.setCommissionCycle(commissionCycle);
//        messageParamDynamicVo.setCommissionAmount(commissionAmount);
//
//        sendMessage(MessageAction.COMMISSION_SUCCESS, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendFindPasswordMobile(User user, String mobileToken) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setMobileToken(mobileToken);
//
//        sendMessage(MessageAction.FIND_PASSWORD_MOBILE, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendFindPasswordEmail(User user, String emailToken) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setUsername(user.getUsername());
//        messageParamDynamicVo.setEmailToken(emailToken);
//
//        sendMessage(MessageAction.FIND_PASSWORD_EMAIL, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendBindEmail(User user, String email, String emailActivatingUrl) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setUsername(user.getUsername());
//        messageParamDynamicVo.setEmailActivatingUrl(emailActivatingUrl);
//
//        Recipient recipient = Recipient.convert(user);
//        recipient.setEmail(email);
//
//        sendMessage(MessageAction.BIND_EMAIL, recipient, messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendQuestionReply(User user, Date questionDate) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setUsername(user.getUsername());
//        messageParamDynamicVo.setQuestionDate(questionDate);
//        sendMessage(MessageAction.QUESTION_REPLY, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendAgentApplySuccess(User user) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setUsername(user.getUsername());
//
//        sendMessage(MessageAction.AGENT_APPLY_SUCCESS, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendUserVipUpdate(User user, String vipName) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setUsername(user.getUsername());
//        messageParamDynamicVo.setVipName(vipName);
//
//        sendMessage(MessageAction.USER_VIP_UPDATE, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendFindAccountPasswordMobile(User user, String mobileToken) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setMobileToken(mobileToken);
//
//        sendMessage(MessageAction.FIND_ACCOUNT_PASSWORD_MOBILE, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendFindAccountPasswordEmail(User user, String emailToken) {
//        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//        messageParamDynamicVo.setUsername(user.getUsername());
//        messageParamDynamicVo.setEmailToken(emailToken);
//
//        sendMessage(MessageAction.FIND_ACCOUNT_PASSWORD_EMAIL, Recipient.convert(user), messageParamDynamicVo);
//    }
//
//    @Override
//    public void sendUserServiceEmail(AdminUser sender, User user, String content) {
//        emailService.send(MessageAction.USER_SERVICE_EMAIL, Recipient.convert(user), content, sender);
//    }
//
//    @Override
//    public void sendUserServiceMobile(AdminUser sender, User user, String content) {
//        smsService.send(MessageAction.USER_SERVICE_MOBILE, Recipient.convert(user), content, null, sender);
//    }
//
//    @Override
//    public void sendUserServiceStationLetter(AdminUser sender, User user, String content) {
//        stationLetterService.send(MessageAction.USER_SERVICE_STATION_LETTER, Recipient.convert(user), content, sender);
//    }
}