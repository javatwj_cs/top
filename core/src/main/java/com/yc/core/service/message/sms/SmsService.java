package com.yc.core.service.message.sms;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import com.yc.common.util.Verification;
import com.yc.core.model.admin.AdminUser;
import com.yc.core.service.message.SendStatus;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.yc.common.constant.Constant;
import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.core.model.message.SmsChannel;
import com.yc.core.model.message.SmsLog;
import com.yc.core.repository.sql.message.SmsChannelRepository;
import com.yc.core.repository.sql.message.SmsLogRepository;
import com.yc.core.service.message.IMessageSmsChannelService;
import com.yc.core.service.message.MessageAction;
import com.yc.core.service.message.MessageParamDynamicVo;
import com.yc.core.service.message.Recipient;

/**
 * 发送短信，任何发送渠道，禁止抛错！！即使出错，保留错误信息
 */
@Service
public class SmsService implements IMessageSmsChannelService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SmsLogRepository smsLogRepository;
    @Autowired
    private SmsChannelRepository smsChannelRepository;

    @Autowired
    private RedisTemplate redisTemplate;

    public List<SmsSender> smsSenderList;

    public SmsService(List<SmsSender> smsSenderList) {
        this.smsSenderList = smsSenderList;
    }

    /**
     * 获取短信发送渠道
     *
     * @param mobile
     * @param messageAction
     * @return
     */
    private SmsChannel getChannel(String mobile, MessageAction messageAction) {
        String key = Constant.SMS_SEND_MESSAGEACTION_CHANNEL + "_" + mobile + "_" + messageAction.name();
        List<SmsChannel> smsChannelList = smsChannelRepository.findByEnableTrue(new Sort(Sort.Direction.ASC, "sortIndex"));

        if (smsChannelList == null || smsChannelList.size() <= 0) {
            throw new ServiceException(ReturnCode.CODE_50101, "短信通道参数不存在");
        }
        if(smsChannelList.size() == 1) {
            return smsChannelList.get(0);
        }

        // 发送时，如果不受发送间隔时间控制，则直接选择第一个通道
        if(!MessageAction.smsSendTimeBound.contains(messageAction)) {
            return smsChannelList.get(0);
        }

        // 如果短时间内重复发送，则选择其他渠道
        SmsChannel temp = smsChannelList.get(0);
        String idStr = (String) redisTemplate.opsForValue().get(key);

        if(StringUtils.isBlank(idStr)) {            // 第一次发送
            redisTemplate.opsForValue().set(key, "" + temp.getId(), Constant.SMS_SEND_TIME_BOUND, TimeUnit.MINUTES);

            return temp;
        }

        String[] ids = idStr.split(",");     // 多次发送
        for (String id : ids) {
            smsChannelList.removeIf(e ->
                Objects.equals(e.getId().toString(), id)
            );
        }
        if(smsChannelList.size() == 0) {
            return temp;
        }

        redisTemplate.opsForValue().set(key, idStr + "," + smsChannelList.get(0).getId(), Constant.SMS_SEND_TIME_BOUND, TimeUnit.MINUTES);
        return smsChannelList.get(0);
    }

    /**
     * 发送短信
     *
     * @param smsChannel
     * @param mobile
     * @param content
     * @return
     */
    private void sendSMS(SmsChannel smsChannel, String mobile, String content, MessageParamDynamicVo messageParamDynamicVo) {
        if (this.smsSenderList == null || this.smsSenderList.size() <= 0) {
            throw new ServiceException(ReturnCode.CODE_50101, "全部短信通道");
        }
        if (smsChannel.getSmsSenderType() == null) {
            throw new ServiceException(ReturnCode.CODE_50101, "短信通道");
        }

        boolean flag = false;
        for (SmsSender smsSender : this.smsSenderList) {
            if (!smsSender.getClass().equals(smsChannel.getSmsSenderType().getSenderType())) {
                continue;
            }

            if (smsSender instanceof SmsSenderContent) {
                SmsSenderContent smsContent = (SmsSenderContent) smsSender;
                smsContent.send(smsChannel, mobile, content);

                flag = true;
                break;
            } else if (smsSender instanceof SmsSenderParam) {
                SmsSenderParam smsParam = (SmsSenderParam) smsSender;
                smsParam.send(smsChannel, mobile, messageParamDynamicVo);

                flag = true;
                break;
            }
        }

        if (!flag) {
            throw new ServiceException(ReturnCode.CODE_50101, "短信通道");
        }
    }

    @Override
    public void send(MessageAction messageAction, Recipient recipient, String content, MessageParamDynamicVo messageParamDynamicVo, AdminUser sender) {
        SmsLog smsLog = new SmsLog();

        smsLog.setMessageAction(messageAction);

        smsLog.setUser(recipient.getUser());
        smsLog.setMobile(recipient.getMobile());
        smsLog.setContent(content);

        smsLog.setSender(sender);
        smsLog.setSenderName(sender == null ? null : sender.getUsername());

        // 发送短信
        SmsChannel smsChannel = null;
        SendStatus status = SendStatus.SENDING;
        try {
            // 如果用户手机号格式不正确，只记录不发送
            if (!Verification.isValidMobile(recipient.getMobile())) {
                throw new ServiceException(ReturnCode.CODE_40002, "手机号不合法");
            }

            smsChannel = getChannel(recipient.getMobile(), messageAction);   // 获取短信渠道
            sendSMS(smsChannel, recipient.getMobile(), content, messageParamDynamicVo); // 发送短信

            status = SendStatus.SUCCESS;
        } catch (Exception e) {
            logger.error("发送短信失败：{}", e);
            status = SendStatus.FAILURE;

            smsLog.setReturnMessage(e.getMessage());
        }

        smsLog.setChannel(smsChannel);
        smsLog.setSendStatus(status);

        smsLogRepository.save(smsLog);
    }

    @Override
    public void send(MessageAction messageAction, Recipient recipient, String content, MessageParamDynamicVo messageParamDynamicVo) {
        send(messageAction, recipient, content, messageParamDynamicVo, null);
    }
}
