package com.yc.core.service.message;

import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.common.util.DateUtil;
import com.yc.common.util.PageUtil;
import com.yc.core.repository.sql.message.EmailLogRepository;
import com.yc.core.repository.sql.message.SmsLogRepository;
import com.yc.core.repository.sql.message.StationLetterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 消息
 */
@Service
public class MessageService {
    @Autowired
    private SmsLogRepository smsLogRepository;
    @Autowired
    private EmailLogRepository emailLogRepository;
    @Autowired
    private StationLetterRepository stationLetterRepository;

    private Page findSmsLogPage(Date startDate, Date endDate, String username, String serviceName, MessageAction messageAction, SendStatus sendStatus, Pageable pageable) {
        return smsLogRepository.findPage(startDate, endDate, username, serviceName, messageAction, sendStatus, pageable);
    }
    private Page findEmailLogPage(Date startDate, Date endDate, String username, String serviceName, MessageAction messageAction, SendStatus sendStatus, Pageable pageable) {
        return emailLogRepository.findPage(startDate, endDate, username, serviceName, messageAction, sendStatus, pageable);
    }
    private Page findStationLetterPage(Date startDate, Date endDate, String username, String serviceName, MessageAction messageAction, Pageable pageable) {
        return stationLetterRepository.findPage(startDate, endDate, username, serviceName, messageAction, pageable);
    }

    public Page findPage(MessageChannel messageChannel, String startDateStr, String endDateStr, String username, String serviceName, MessageAction messageAction, SendStatus sendStatus, Integer page, Integer pageSize) {
        if(messageChannel == null) {
            throw new ServiceException(ReturnCode.CODE_40003, "通道类型");
        }

        Date startDate = DateUtil.strToDate(startDateStr, null);
        Date endDate = DateUtil.strToDate(endDateStr, null);
        Pageable pageable = (page == null && pageSize == null) ? null : PageUtil.getPageable(page, pageSize);

        if(messageChannel == MessageChannel.SMS) {
            return findSmsLogPage(startDate, endDate, username, serviceName, messageAction, sendStatus, pageable);
        }
        if(messageChannel == MessageChannel.EMAIL) {
            return findEmailLogPage(startDate, endDate, username, serviceName, messageAction, sendStatus, pageable);
        }
        if(messageChannel == MessageChannel.STATION_LETTER) {
            return findStationLetterPage(startDate, endDate, username, serviceName, messageAction, pageable);
        }

        return null;
    }



}
