package com.yc.core.service.message;

import com.yc.common.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 消息动态内容
 */
public class MessageParamDynamicVo {

    private String username;
    private String mobileToken;
    private BigDecimal depositAmount;
    private BigDecimal balance;
    private Date withdrawApplyDate;
    private BigDecimal withdrawAmount;
    private Date favorableApplyDate;
    private String favorableName;
    private BigDecimal rebateAmount;
    private String commissionCycle;
    private BigDecimal commissionAmount;
    private String emailToken;
    private String emailActivatingUrl;
    private Date questionDate;
    private String vipName;

    public String getMobileToken() {
        return mobileToken;
    }

    public void setMobileToken(String mobileToken) {
        this.mobileToken = mobileToken;
    }

    public String getEmailToken() {
        return emailToken;
    }

    public void setEmailToken(String emailToken) {
        this.emailToken = emailToken;
    }

    public String getEmailActivatingUrl() {
        return emailActivatingUrl;
    }

    public void setEmailActivatingUrl(String emailActivatingUrl) {
        this.emailActivatingUrl = emailActivatingUrl;
    }

    public Date getQuestionDate() {
        return questionDate;
    }

    public void setQuestionDate(Date questionDate) {
        this.questionDate = questionDate;
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public BigDecimal getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Date getWithdrawApplyDate() {
        return withdrawApplyDate;
    }

    public void setWithdrawApplyDate(Date withdrawApplyDate) {
        this.withdrawApplyDate = withdrawApplyDate;
    }

    public BigDecimal getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(BigDecimal withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public Date getFavorableApplyDate() {
        return favorableApplyDate;
    }

    public void setFavorableApplyDate(Date favorableApplyDate) {
        this.favorableApplyDate = favorableApplyDate;
    }

    public String getFavorableName() {
        return favorableName;
    }

    public void setFavorableName(String favorableName) {
        this.favorableName = favorableName;
    }

    public BigDecimal getRebateAmount() {
        return rebateAmount;
    }

    public void setRebateAmount(BigDecimal rebateAmount) {
        this.rebateAmount = rebateAmount;
    }

    public String getCommissionCycle() {
        return commissionCycle;
    }

    public void setCommissionCycle(String commissionCycle) {
        this.commissionCycle = commissionCycle;
    }

    public BigDecimal getCommissionAmount() {
        return commissionAmount;
    }

    public void setCommissionAmount(BigDecimal commissionAmount) {
        this.commissionAmount = commissionAmount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 过滤文案动态内容
     * @param content
     * @return
     */
    public String filter(String content) {
        if(StringUtils.isBlank(content)) {
            return content;
        }

        Field[] fields = this.getClass().getDeclaredFields();
        Object value;
        for(Field field : fields) {
            try {
                value = field.get(this);
            } catch (IllegalAccessException e) {
                value = null;
            }

            // 数据为空不进行转换
            if(value == null) {
                continue;
            }
            // 对应枚举不存在不予转换
            if(value instanceof Date) {
                value = DateFormatUtils.format((Date)value, DateUtil.NORMAL_PATTERN);
            }

            content = content.replace("${" + field.getName() + "}", value.toString());
        }

        return content;
    }

    public static void main(String[] args) {
        MessageParamDynamicVo messageParamDynamicVo = new MessageParamDynamicVo();
//
//        messageParamDynamicVo.setRebateAmount(new BigDecimal("1111"));
//        messageParamDynamicVo.setEmailToken("fdsfdsfds");
//        messageParamDynamicVo.setQuestionDate(new Date());

        String result = messageParamDynamicVo.filter("尊敬的用户[questionDate]，你的账户[emailToken]新到账返利金额为[rebateAmount]，还请及时查收账户");

        System.out.println(result);
    }
}
