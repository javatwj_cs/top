package com.yc.core.service.message;

import java.util.ArrayList;
import java.util.List;

/**
 * 消息日志类型
 */
public enum MessageAction {

    REGISTER_MOBILE("手机注册"),
    BIND_WX("绑定微信"),
//    REGISTER_SUCCESS("注册成功"),
//    DEPOSIT_OFFLINE_SUCCESS("存款成功（线下）"),
//    WITHDRAW_APPLY("提现申请"),
//    WITHDRAW_SUCCESS("提现成功"),
//    FAVORABLE_SUCCESS("优惠到账/成功"),
//    REBATE_SUCCESS("洗码到账"),
//    COMMISSION_SUCCESS("佣金到账"),
//    FIND_PASSWORD_MOBILE("手机找回密码"),
//    FIND_PASSWORD_EMAIL("邮箱找回密码"),
//    BIND_EMAIL("绑定邮箱"),
//    QUESTION_REPLY("留言回复"),
//    AGENT_APPLY_SUCCESS("代理申请成功"),
//    USER_VIP_UPDATE("用户会员等级变动"),
//    UPDATE_MOBILE("更新手机号"),
//    FIND_ACCOUNT_PASSWORD_MOBILE("手机找回交易密码"),
//    FIND_ACCOUNT_PASSWORD_EMAIL("邮箱找回交易密码"),
//    USER_SERVICE_EMAIL("客服跟进记录-发送邮件"),
//    USER_SERVICE_MOBILE("客服跟进记录-发送手机短信"),
//    USER_SERVICE_STATION_LETTER("客服跟进记录-发送站内信"),

    ;

    private String subject;

    MessageAction(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public static List<MessageAction> smsSendTimeBound = new ArrayList<>();     // sms发送，需要进行时间限制的messageAction
    public static List<MessageAction> emailSendTimeBound = new ArrayList<>();   // email发送，需要进行时间限制的messageAction

    static {
        smsSendTimeBound.add(MessageAction.REGISTER_MOBILE);
        smsSendTimeBound.add(MessageAction.BIND_WX);
//        smsSendTimeBound.add(MessageAction.FIND_PASSWORD_MOBILE);
//        smsSendTimeBound.add(MessageAction.UPDATE_MOBILE);
//        smsSendTimeBound.add(MessageAction.FIND_ACCOUNT_PASSWORD_MOBILE);
//
//        emailSendTimeBound.add(MessageAction.FIND_PASSWORD_EMAIL);
//        emailSendTimeBound.add(MessageAction.BIND_EMAIL);
//        emailSendTimeBound.add(MessageAction.FIND_ACCOUNT_PASSWORD_EMAIL);
    }
}
