package com.yc.core.service;

import java.util.List;

import javax.annotation.Resource;
import javax.mail.internet.ContentType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.yc.common.constant.BannerContentType;
import com.yc.common.constant.BannerPlace;
import com.yc.common.constant.Terminal;
import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.common.util.PageUtil;
import com.yc.core.model.Banner;
import com.yc.core.model.admin.AdminUser;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.loader.custom.Return;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.yc.core.model.Settings;
import com.yc.core.repository.sql.BannerRepository;

@Service
public class BannerService {

    Logger logger = LogManager.getLogger(this.getClass());

    @Resource
    private BannerRepository bannerRepository;

    private void checkParams(Banner params) {
        if(StringUtils.isBlank(params.getTitle())) {
            throw new ServiceException(ReturnCode.CODE_40004, "标题");
        }
        if(StringUtils.isBlank(params.getImage())) {
            throw new ServiceException(ReturnCode.CODE_40004, "图片");
        }
        if(params.getContentType() == null) {
            throw new ServiceException(ReturnCode.CODE_40004, "Banner类型");
        }
        if(params.getPlace() == null) {
            throw new ServiceException(ReturnCode.CODE_40004, "Banner位置");
        }
        if(params.getTerminal() == null) {
            throw new ServiceException(ReturnCode.CODE_40004, "终端");
        }
        if(params.getEnable() == null) {
            throw new ServiceException(ReturnCode.CODE_40004, "是否启用");
        }
        if(params.getSortIndex() == null || params.getSortIndex() < 0) {
            throw new ServiceException(ReturnCode.CODE_40004, "排序");
        }
    }

    public Banner saveOrUpdate(Banner params, AdminUser operator) {
        checkParams(params);

        Banner banner = new Banner();
        if(params.getId() != null && params.getId() >= 0) {
            banner = bannerRepository.findOne(params.getId());
            if(banner == null) {
                throw new ServiceException(ReturnCode.CODE_40005, "Banner");
            }
        }

        banner.setTitle(params.getTitle());
        banner.setImage(params.getImage());
        banner.setContentType(params.getContentType());
        banner.setContent(params.getContent());
        banner.setPlace(params.getPlace());
        banner.setTerminal(params.getTerminal());
        banner.setEnable(params.getEnable());
        banner.setSortIndex(params.getSortIndex());
        banner.setShutTimes(0);

        banner.setOperatorName(operator.getUsername());

        return bannerRepository.save(banner);
    }

    /**
     * 分页查询banner
     * @param enable
     * @param title
     * @param place
     * @param terminal
     * @param pno
     * @param psize
     * @return
     */
    public Page findPage(String title, BannerPlace place, Terminal terminal, Boolean enable, Integer pno, Integer psize) {
        return bannerRepository.findPage(title, place, terminal, enable, PageUtil.getPageable(pno, psize));
    }

    /**
     * 根据位置和终端，查询启动的banner
     * @param place
     * @param terminal
     * @return
     */
    public List<Banner> getList(BannerPlace place, Terminal terminal) {
        return bannerRepository.getByPlaceAndTerminalAndEnableTrue(place, terminal);
    }
}
