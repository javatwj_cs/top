package com.yc.core.service.message;


import com.yc.core.model.user.User;

/**
 * 收信人
 */
public class Recipient {

    private User user;

    private String email;
    private String mobile;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public static Recipient convert(User user) {
        if(user == null) {
            return new Recipient();
        }

        Recipient recipient = new Recipient();

        recipient.setUser(user);
        recipient.setEmail(user.getEmail());
        recipient.setMobile(user.getMobile());

        return recipient;
    }
}
