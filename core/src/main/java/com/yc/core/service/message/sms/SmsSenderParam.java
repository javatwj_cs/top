package com.yc.core.service.message.sms;


import com.yc.core.model.message.SmsChannel;
import com.yc.core.service.message.MessageParamDynamicVo;

/**
 * 短信发送 - param
 */
public interface SmsSenderParam extends SmsSender {
    void send(SmsChannel smsChannel, String mobile, MessageParamDynamicVo messageParamDynamicVo);
}
