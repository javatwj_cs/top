package com.yc.core.service.message.sms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.core.model.message.SmsChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class Emay implements SmsSenderContent {

    private static final String APP_ID = "APP_ID";
    private static final byte[] SECRET_KEY = "SECRET_KEY".getBytes(StandardCharsets.UTF_8);

    private static final String BASE_URL = "BASE_URL";

    private static final String AES = "AES";
    private static final String ALGORITHM = "AES/ECB/PKCS5Padding";

    private static final int REQUEST_VALID_PERIOD = 30;

    private final Logger logger = LogManager.getLogger(getClass());

    private final Cipher encryptor;
    private final Cipher decryptor;

    @Value("${project.sms.corporate-signature}")
    private String corporateSignature;

    @Autowired
    private RestTemplate restTemplate;
    @Resource
    private ObjectMapper objectMapper;

    public Emay() {
        this.encryptor = newCipher(ALGORITHM);
        this.decryptor = newCipher(ALGORITHM);
    }

    private byte[] excute(String path, Map<String, Object> body) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("appId", APP_ID);

        byte[] data = null;
        try {
            data = objectMapper.writeValueAsBytes(body);
        } catch (JsonProcessingException e) {
            logger.error("格式转换异常，body:{}", body);

            throw new ServiceException(ReturnCode.CODE_50101, "格式转换失败");
        }
        data = encrypt(data);

        String url = BASE_URL + path;
        RequestEntity<byte[]> request = new RequestEntity<>(data, headers, HttpMethod.POST, URI.create(url));
        ResponseEntity<byte[]> response = restTemplate.exchange(request, byte[].class);

        if (response.getStatusCode() != HttpStatus.OK) {
            throw new ServiceException(ReturnCode.CODE_50101, "发送失败");
        }

        String code = response.getHeaders().getFirst("result");
        if (!"SUCCESS".equals(code)) {
            logger.warn("亿美短信发送失败，状态码：{}", code);
            throw new ServiceException(ReturnCode.CODE_50101, "亿美短信发送失败，状态码：" + code);
        }

        return response.getBody() != null ? decrypt(response.getBody()) : null;
    }

    @Override
    public void send(SmsChannel smsChannel, String mobile, String content) {
        send(mobile, corporateSignature + content);
    }

    /**
     * 发送短信。
     *
     * @param mobile 手机号码
     * @param content 短信内容，255个字符（亿美限制：UTF-8编码，最多500个汉字或1000个纯英文）
     */
    public void send(String mobile, String content) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("mobile", mobile);
        body.put("content", content);
        body.put("customSmsId", ObjectId.get().toHexString());
        body.put("requestTime", System.currentTimeMillis());
        body.put("requestValidPeriod", REQUEST_VALID_PERIOD);

        byte[] resp = excute("sendSingleSMS", body);

        String result = new String(resp);
        logger.info("Send result: {}", result);
    }

    /**
     * 获取账户余额。
     */
    public int getBalance() {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("requestTime", System.currentTimeMillis());
        body.put("requestValidPeriod", REQUEST_VALID_PERIOD);

        byte[] resp = excute("getBalance", body);

        try {
            JsonNode json = objectMapper.readTree(resp);
            return json.get("balance").asInt();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Cipher newCipher(String algorithm) {
        try {
            return Cipher.getInstance(algorithm);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new IllegalArgumentException("Not a valid encryption algorithm", e);
        }
    }

    private byte[] encrypt(byte[] bytes) {
        synchronized (this.encryptor) {
            initCipher(this.encryptor, Cipher.ENCRYPT_MODE, new SecretKeySpec(SECRET_KEY, AES));
            return doFinal(this.encryptor, bytes);
        }
    }

    private byte[] decrypt(byte[] encryptedBytes) {
        synchronized (this.decryptor) {
            initCipher(this.decryptor, Cipher.DECRYPT_MODE, new SecretKeySpec(SECRET_KEY, AES));
            return doFinal(this.decryptor, encryptedBytes);
        }
    }

    private void initCipher(Cipher cipher, int mode, SecretKeySpec secretKey) {
        try {
            cipher.init(mode, secretKey);
        } catch (InvalidKeyException e) {
            throw new IllegalArgumentException("Unable to initialize due to invalid secret key", e);
        }
    }

    private byte[] doFinal(Cipher cipher, byte[] input) {
        try {
            return cipher.doFinal(input);
        } catch (IllegalBlockSizeException e) {
            throw new IllegalStateException("Unable to invoke Cipher due to illegal block size", e);
        } catch (BadPaddingException e) {
            throw new IllegalStateException("Unable to invoke Cipher due to bad padding", e);
        }
    }

}
