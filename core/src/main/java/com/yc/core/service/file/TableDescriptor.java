package com.yc.core.service.file;

import java.util.ArrayList;
import java.util.List;

/**
 * 实体描述。
 *
 * @param <T> 实体类型
 */
public abstract class TableDescriptor<T> {

    private final List<ColumnDescriptor<T, ?>> columnDescriptors = new ArrayList<>();

    protected final String name;

    private int maxColumn = -1;

    public TableDescriptor(String name) {
        this.name = name;
        initColumnDescriptors();
    }

    protected void add(ColumnDescriptor<T, ?> cd) {
        if (cd.getColumn() < 0) {
            cd.setColumn(++maxColumn);
        } else if (cd.getColumn() > maxColumn) {
            maxColumn = cd.getColumn();
        }
        columnDescriptors.add(cd);
    }

    public String getName() {
        return name;
    }

    public List<ColumnDescriptor<T, ?>> getColumnDescriptors() {
        return columnDescriptors;
    }

    public abstract void initColumnDescriptors();

    public abstract T newInstance();

}
