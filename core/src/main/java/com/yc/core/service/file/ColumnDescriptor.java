package com.yc.core.service.file;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * 实体字段描述。
 *
 * @param <T> 实体类型
 * @param <R> 字段类型
 */
public class ColumnDescriptor<T, R> {

    private String name;
    private int column = -1;

    private final Class<R> type;
    private final Function<T, R> getter;
    private final BiConsumer<T, R> setter;

    public ColumnDescriptor(String name, Class<R> type, Function<T, R> getter) {
        this(name, type, getter, null);
    }

    public ColumnDescriptor(String name, Class<R> type, BiConsumer<T, R> setter) {
        this(name, type, null, setter);
    }

    public ColumnDescriptor(String name, Class<R> type, Function<T, R> getter, BiConsumer<T, R> setter) {
        this.name = name;
        this.type = type;
        this.getter = getter;
        this.setter = setter;
    }

    @SuppressWarnings("unchecked")
    public ColumnDescriptor(String name, Class<T> parent, String propertyName) {
        this.name = name;
        try {
            PropertyDescriptor pd = new PropertyDescriptor(propertyName, parent);
            this.type = (Class<R>) pd.getPropertyType();
            this.getter = makeGetter(pd);
            this.setter = makeSetter(pd);
        } catch (IntrospectionException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Function<T, R> makeGetter(PropertyDescriptor pd) {
        return o -> {
            try {
                @SuppressWarnings("unchecked")
                R result = (R) pd.getReadMethod().invoke(o);
                return result;
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        };
    }

    private BiConsumer<T, R> makeSetter(PropertyDescriptor pd) {
        return (o, v) -> {
            try {
                pd.getWriteMethod().invoke(o, v);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        };
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public Class<R> getType() {
        return type;
    }

    public R getValue(T t) {
        return getter.apply(t);
    }

    @SuppressWarnings("unchecked")
    public void setValue(T t, Object v) {
        setter.accept(t, (R) v);
    }

}
