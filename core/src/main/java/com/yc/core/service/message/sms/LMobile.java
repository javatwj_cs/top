package com.yc.core.service.message.sms;

import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.core.model.message.SmsChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 乐信。
 *
 * @see http://www.lmobile.cn/ch/api.html
 *
 */
@Component
public class LMobile implements SmsSenderContent {

    // http://jiekou.51welink.com/submitdata/service.asmx
    private static final String BASE_URL = "BASE_URL";
    private static final String SEND_URL = BASE_URL + "SEND_URL";

    private static final Pattern CODE_PATTERN = Pattern.compile(".*<State>(\\d+)</State>.*", Pattern.DOTALL);

    private final Logger logger = LogManager.getLogger(getClass());

    @Value("${project.sms.corporate-signature}")
    private String corporateSignature;

    @Resource
    private RestTemplate restTemplate;

    public void send(String mobile, String content) {

        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("sname", "dlmtysm0");
        body.add("spwd", "LINlin20100214");
        body.add("scorpid", "");
        body.add("sprdid", "1012818");
        body.add("sdst", mobile);
        body.add("smsg", content);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        ResponseEntity<String> response = restTemplate.exchange(SEND_URL, HttpMethod.POST, new HttpEntity<>(body, headers), String.class);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new ServiceException(ReturnCode.CODE_50101, "短信发送失败");
        }

        /*
        <?xml version="1.0" encoding="utf-8"?>
        <CSubmitState xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://tempuri.org/">
          <State>0</State>
          <MsgID>1708101708404235112</MsgID>
          <MsgState>提交成功</MsgState>
          <Reserve>1</Reserve>
        </CSubmitState>
         */
        String result = response.getBody();
        Objects.nonNull(result);

        Matcher m = CODE_PATTERN.matcher(response.getBody());
        if (!m.matches()) {
            throw new RuntimeException("乐信短信返回值结构异常：" + result);
        }

        int state = Integer.parseInt(m.group(1));
        if (state != 0) {
            logger.warn("乐信短信发送失败：{}", response.getBody());
            throw new RuntimeException("乐信短信发送失败，状态码：" + state);
        }

    }

    @Override
    public void send(SmsChannel smsChannel, String mobile, String content) {
        send(mobile, corporateSignature + content);
    }

}
