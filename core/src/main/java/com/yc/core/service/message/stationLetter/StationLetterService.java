package com.yc.core.service.message.stationLetter;

import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.common.util.PageUtil;
import com.yc.core.model.admin.AdminUser;
import com.yc.core.model.message.StationLetter;
import com.yc.core.repository.sql.message.StationLetterRepository;
import com.yc.core.service.message.IMessageChannelService;
import com.yc.core.service.message.MessageAction;
import com.yc.core.service.message.Recipient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

/**
 * 站内信
 */
@Service("stationLetterService")
public class StationLetterService implements IMessageChannelService {

    @Autowired
    private StationLetterRepository stationLetterRepository;

    @Override
    public void send(MessageAction messageAction, Recipient recipient, String content) {
        send(messageAction, recipient, content, null);
    }

    @Override
    public void send(MessageAction messageAction, Recipient recipient, String content, AdminUser sender) {
        StationLetter stationLetter = new StationLetter();

        stationLetter.setUser(recipient.getUser());
        stationLetter.setContent(content);

        stationLetter.setSender(sender);
        stationLetter.setSenderName(sender == null ? null : sender.getUsername());
        stationLetter.setMessageAction(messageAction);

        stationLetter.setStatus(Boolean.FALSE);

        stationLetterRepository.save(stationLetter);
    }

    /**
     * 分页查询用户站内信
     * @param userId
     * @param page
     * @param pageSize
     * @return
     */
    public Page<StationLetter> getPage(Long userId, Integer page, Integer pageSize) {
        if(userId == null || userId <= 0) {
            throw new ServiceException(ReturnCode.CODE_40004, "用户");
        }

        return stationLetterRepository.findByUserId(userId, PageUtil.getPageable(page, pageSize));
    }

    /**
     * 站内信，已读
     * @param userId
     * @param id
     */
    public void haveRead(Long userId, Long id) {
        if(id == null || id <= 0) {
            throw new ServiceException(ReturnCode.CODE_40004, "站内信");
        }

        StationLetter stationLetter = stationLetterRepository.findOne(id);
        if(stationLetter == null) {
            throw new ServiceException(ReturnCode.CODE_40004, "站内信");
        }
        if(!stationLetter.getUser().getId().equals(userId)) {
            throw new ServiceException(ReturnCode.CODE_40004, "站内信");
        }

        stationLetter.setStatus(Boolean.TRUE);
    }

    public Integer getUnreadCount(Long userId){
        if(userId == null || userId <= 0) {
            return 0;
        }

        return stationLetterRepository.countByUserIdAndStatus(userId, Boolean.FALSE);
    }
}
