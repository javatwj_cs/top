package com.yc.core.service;

import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.core.model.Settings;
import com.yc.core.repository.sql.SettingsRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SettingsService {

    Logger logger = LogManager.getLogger(this.getClass());

    @Resource
    private SettingsRepository settingsRepository;

    public List<Settings> list() {
        return settingsRepository.findAll();
    }

    public Settings findName(String name) {
        return settingsRepository.findByName(name);
    }

    public List<Settings> findByNameStartingWith(String name) {
        return settingsRepository.findByNameStartingWith(name);
    }

    public void update(Settings settings) {
        if(settings == null || StringUtils.isBlank(settings.getName())) {
            throw new ServiceException(ReturnCode.CODE_40004, "settings");
        }

        Settings entity = findName(settings.getName());
        if (entity == null) {
            throw new ServiceException(ReturnCode.CODE_40005, settings.getName());
        }

        entity.setValue(settings.getValue());
        entity.setRemark(settings.getRemark());

        settingsRepository.save(entity);
    }



}
