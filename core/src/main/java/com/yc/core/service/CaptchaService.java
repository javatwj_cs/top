package com.yc.core.service;

import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.common.util.Verification;
import com.yc.core.cache.ImgTokenCache;
import com.yc.core.cache.MobileToken;
import com.yc.core.cache.MobileTokenCache;
import com.yc.core.model.user.User;
import com.yc.core.service.message.MessageActionService;
import com.yc.core.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class CaptchaService {

    @Autowired
    private MobileTokenCache mobileTokenCache;
    @Autowired
    private ImgTokenCache imgTokenCache;

    @Autowired
    private MessageActionService messageActionService;
    @Autowired
    private UserService userService;

    public long sendRegisterToken(String mobile, String imgKey, String imgToken) {
        if(!Verification.isValidMobile(mobile)) {
            throw new ServiceException(ReturnCode.CODE_40004, "手机号码");
        }

        // 验证图片验证码
        if(!imgTokenCache.checkToken(imgKey, imgToken)) {
            throw new ServiceException(ReturnCode.CODE_40007, "图片验证码");
        }

        User user = userService.getUserByMobile(mobile);
        if(user != null && user.getId() > 0) {
            throw new ServiceException(ReturnCode.CODE_40006, "手机号码");
        }

        // 缓存手机验证码，并生成手机验证码
        MobileToken mobileToken = mobileTokenCache.set(mobile);

        // 发送短信
        messageActionService.sendRegisterMobile(mobile, mobileToken.getToken());


        return mobileToken.getReTime();
    }

    public long sendWxToken(String mobile, String imgKey, String imgToken) {
        if(!Verification.isValidMobile(mobile)) {
            throw new ServiceException(ReturnCode.CODE_40004, "手机号码");
        }

        // 验证图片验证码
        if(!imgTokenCache.checkToken(imgKey, imgToken)) {
            throw new ServiceException(ReturnCode.CODE_40007, "图片验证码");
        }

        User user = userService.getUserByMobile(mobile);
        if(!(user != null && user.getId() > 0)) {
            throw new ServiceException(ReturnCode.CODE_40005, "用户");
        }

        // 缓存手机验证码，并生成手机验证码
        MobileToken mobileToken = mobileTokenCache.set(mobile);

        // 发送短信
        messageActionService.sendBindWx(user, mobileToken.getToken());

        return mobileToken.getReTime();
    }

}
