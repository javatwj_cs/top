package com.yc.core.service.file;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 辅助读写Excel文档。
 *
 */
public class ExcelHelper implements AutoCloseable {

    private final Workbook workbook;

    private int titleRow = 0;               // 标题行
    private int startRow = titleRow + 1;    // 默认跳过标题行

    public ExcelHelper() {
        this.workbook = new XSSFWorkbook();
    }

    public ExcelHelper(Workbook workbook) {
        this.workbook = workbook;
    }

    /**
     * 解析“.xlsx”格式Excel文档
     *
     * @param is InputStream
     * @throws IOException IOException
     */
    public ExcelHelper(InputStream is) throws IOException {
        this.workbook = new XSSFWorkbook(is);
    }

    public Workbook getWorkbook() {
        return workbook;
    }

    /**
     * 读取第一个Sheet内容并转为实体列表。
     *
     * @param td 实体描述
     * @return 实体列表
     */
    public <T> List<T> readSheet(TableDescriptor<T> td) {
        return readSheet(null, td);
    }

    /**
     * 读取指定Sheet内容并转为实体列表。
     *
     * 如果某一行第一列为空，则会跳过该行。
     *
     * @param sheetName Sheet名称
     * @param td 实体描述
     * @return 实体列表
     */
    public <T> List<T> readSheet(String sheetName, TableDescriptor<T> td) {

        Sheet sh = sheetName == null ? workbook.getSheetAt(0) : workbook.getSheet(sheetName);
        if (sh == null) {
            throw new IllegalArgumentException("无法获取到指定名称为“" + sheetName + "”的Sheet");
        }

        List<T> list = new ArrayList<>(sh.getLastRowNum());
        for (int i = startRow; i <= sh.getLastRowNum(); i++) {

            Row r = sh.getRow(i);
            if (r == null || r.getCell(0) == null) {
                continue;
            }

            T t = td.newInstance();
            list.add(t);

            for (ColumnDescriptor<T, ?> cd : td.getColumnDescriptors()) {

                Cell c = r.getCell(cd.getColumn());
                if (c == null) {
                    continue;
                }

                Class<?> type = cd.getType();

                if (c.getCellTypeEnum() == CellType.BLANK && String.class.equals(type)) {
                    cd.setValue(t, "");
                } else if (String.class.equals(type)) {
                    cd.setValue(t, c.getStringCellValue());
                } else if (Integer.class.equals(type)) {
                    cd.setValue(t, (int) c.getNumericCellValue());
                } else if (Long.class.equals(type)) {
                    cd.setValue(t, c.getNumericCellValue());
                } else if (Float.class.equals(type)) {
                    cd.setValue(t, (float) c.getNumericCellValue());
                } else if (Double.class.equals(type)) {
                    cd.setValue(t, c.getNumericCellValue());
                } else if (BigDecimal.class.equals(type)) {
                    if (c.getCellTypeEnum() == CellType.NUMERIC) {
                        cd.setValue(t, new BigDecimal(c.getNumericCellValue()));
                    } else {
                        cd.setValue(t, new BigDecimal(c.getStringCellValue()));
                    }
                } else if (Boolean.class.equals(type)) {
                    cd.setValue(t, c.getBooleanCellValue());
                } else if (java.sql.Date.class.equals(type)) {
                    cd.setValue(t, new java.sql.Date(c.getDateCellValue().getTime()));
                } else if (java.util.Date.class.equals(type)) {
                    cd.setValue(t, c.getDateCellValue());
                } else if (type.isEnum()) {
                    String v = c.getStringCellValue();
                    cd.setValue(t, getEnum(type, v));
                } else {
                    throw new IllegalArgumentException("Unsupported type " + type);
                }
            }

        }

        return list;
    }

    private Object getEnum(Class<?> clz, String name) {
        for (Object o : clz.getEnumConstants()) {
            if (((Enum<?>) o).name().equals(name)) {
                return o;
            }
        }
        return null;
    }

    /**
     * 将指定实体集合写入文档。
     *
     * @param objects 实体集合
     * @param td 实体描述
     */
    public <T> void writeSheet(Collection<T> objects, TableDescriptor<T> td) {
        this.writeSheet(objects, null, td);
    }

    /**
     * 将指定实体集合写入文档。
     *
     * @param objects 实体集合
     * @param sheetName Sheet名称
     * @param td 实体描述
     */
    public <T> void writeSheet(Collection<T> objects, String sheetName, TableDescriptor<T> td) {

        // 日期
        CreationHelper createHelper = workbook.getCreationHelper();
        CellStyle shortFormat = workbook.createCellStyle();
        shortFormat.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd"));
        CellStyle longFormat = workbook.createCellStyle();
        longFormat.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd HH:mm:ss"));

        if (sheetName == null) {
            sheetName = td.getName();
        }
        Sheet sh = workbook.createSheet(sheetName);

        // 写入标题
        Row tr = sh.createRow(titleRow);
        for (ColumnDescriptor<T, ?> cd : td.getColumnDescriptors()) {
            Cell c = tr.createCell(cd.getColumn());
            c.setCellValue(cd.getName());
        }

        // 写入数据
        int curRow = startRow;
        for (T t : objects) {
            Row r = sh.createRow(curRow++);
            for (ColumnDescriptor<T, ?> cd : td.getColumnDescriptors()) {
                Object v = cd.getValue(t);
                if (v == null) {
                    continue;
                }

                Cell c = r.createCell(cd.getColumn());
                Class<?> type = cd.getType();

                if (String.class.equals(type)) {
                    c.setCellValue((String) v);
                } else if (Integer.class.equals(type)) {
                    c.setCellValue((Integer) v);
                } else if (Long.class.equals(type)) {
                    c.setCellValue((Long) v);
                } else if (Float.class.equals(type)) {
                    c.setCellValue(new Double(v.toString()));
                } else if (Double.class.equals(type)) {
                    c.setCellValue((Double) v);
                } else if (BigDecimal.class.equals(type)) {
                    c.setCellValue(v.toString());
                } else if (Boolean.class.equals(type)) {
                    c.setCellValue((Boolean) v);
                } else if (java.sql.Date.class.equals(type)) {
                    c.setCellValue((java.sql.Date) v);
                    c.setCellStyle(shortFormat);
                } else if (java.util.Date.class.equals(type)) {
                    c.setCellValue((java.util.Date) v);
                    c.setCellStyle(longFormat);
                } else if (type.isEnum()) {
                    c.setCellValue(((Enum<?>) v).name());
                }  else {
                    throw new IllegalArgumentException("Unsupported type " + type);
                }
            }
        }

        // 自动列宽
        for (ColumnDescriptor<T, ?> cd : td.getColumnDescriptors()) {
            sh.autoSizeColumn(cd.getColumn());
        }

    }

    public void writeTo(OutputStream outputStream) throws IOException {
        workbook.write(outputStream);
    }

    @Override
    public void close() {
        if (workbook == null) {
            return;
        }
        try {
            workbook.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


//    @GetMapping("/log/download")
//    public StreamingResponseBody downloadOfflineList(MessageChannel messageChannel, String startDateStr, String endDateStr, String username, String serviceName, MessageAction messageAction, SendStatus sendStatus,  HttpServletResponse response) throws UnsupportedEncodingException {
//        List list = messageService.findPage(messageChannel, startDateStr, endDateStr, username, serviceName, messageAction, sendStatus, null, null).getContent();
//
//        response.setContentType("application/vnd.ms-excel;charset=utf-8");
//        response.setHeader("Content-Disposition", "attachment;filename=" + new String("消息发送记录.xlsx".getBytes(), "iso-8859-1"));
//
//        return outputStream -> {
//            try (ExcelHelper helper = new ExcelHelper()) {
//                if(messageChannel == MessageChannel.SMS) {
//                    helper.writeSheet(list, SmsLogDescriptor.INSTANCE);
//                }else if(messageChannel == MessageChannel.EMAIL) {
//                    helper.writeSheet(list, EmailLogDescriptor.INSTANCE);
//                }else if(messageChannel == MessageChannel.STATION_LETTER) {
//                    helper.writeSheet(list, StationLetterDescriptor.INSTANCE);
//                }
//                helper.writeTo(outputStream);
//            }
//        };
//    }

}
