package com.yc.core.service.message.sms;

/**
 *
 */
public enum SmsSenderType {

    EMAY(Emay.class),       // 亿美
    LMOBILE(LMobile.class), // 乐信
    YUNPIAN(Yunpian.class), // 云片
    SIMPLE(SimpleSms.class), // 云片

    ;

    private final Class<? extends SmsSender> senderType;

    private SmsSenderType(Class<? extends SmsSender> senderType) {
        this.senderType = senderType;
    }

    public Class<? extends SmsSender> getSenderType() {
        return senderType;
    }

}
