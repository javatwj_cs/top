package com.yc.core.service.message;


import com.yc.core.model.admin.AdminUser;

/**
 * 发送消息渠道 - sms标准
 */
public interface IMessageSmsChannelService {
    public void send(MessageAction messageAction, Recipient recipient, String content, MessageParamDynamicVo messageParamDynamicVo);
    public void send(MessageAction messageAction, Recipient recipient, String content, MessageParamDynamicVo messageParamDynamicVo, AdminUser sender);
}
