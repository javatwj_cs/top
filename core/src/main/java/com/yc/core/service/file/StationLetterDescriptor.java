package com.yc.core.service.file;

import java.util.Date;

import com.yc.core.model.message.StationLetter;

public class StationLetterDescriptor extends TableDescriptor<StationLetter> {

    public static final StationLetterDescriptor INSTANCE = new StationLetterDescriptor();

    public StationLetterDescriptor() {
        super("消息发送记录");
    }


    @Override
    public void initColumnDescriptors() {
        add(new ColumnDescriptor<>("ID", Long.class, entity -> entity.getId()));
        add(new ColumnDescriptor<>("内容", String.class, entity -> entity.getContent()));
        add(new ColumnDescriptor<>("用户名", String.class, entity -> entity.getUser() == null ? null : entity.getUser().getUsername()));
        add(new ColumnDescriptor<>("真实姓名", String.class, entity -> entity.getUser() == null ? null : entity.getUser().getName()));
        add(new ColumnDescriptor<>("发送人", String.class, entity -> entity.getSenderName() == null ? "system" : entity.getSenderName()));
        add(new ColumnDescriptor<>("发送时间", Date.class, entity -> entity.getCreateTime()));
        add(new ColumnDescriptor<>("通道类型", String.class, entity -> "站内信"));
        add(new ColumnDescriptor<>("消息类型", String.class, entity -> entity.getMessageAction().getSubject()));
        add(new ColumnDescriptor<>("状态", String.class, entity -> "发送成功"));
    }

    @Override
    public StationLetter newInstance() {
        return new StationLetter();
    }

}
