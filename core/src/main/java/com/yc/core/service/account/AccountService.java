package com.yc.core.service.account;

import com.yc.common.constant.AccountType;
import com.yc.common.constant.BillType;
import com.yc.core.model.account.AccountBill;
import com.yc.core.model.user.User;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by tianjie on 17/8/20.
 */
@Service
public class AccountService extends AbstractAccountService {

    public AccountBill deposit(User user, BigDecimal amount) {
        return super.increment(user, AccountType.AMOUNT, BillType.DEPOSIT, amount);
    }

    public AccountBill withdraw(User user, BigDecimal amount) {
        return super.reduction(user, AccountType.AMOUNT, BillType.WITHDRAW, amount);
    }
}
