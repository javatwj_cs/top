package com.yc.core.service.message;

/**
 * 短信日志状态
 */
public enum SendStatus {

    SENDING("发送中"),       // 0
    FAILURE("发送失败"),       // 1 发送失败
    SUCCESS("发送成功")        // 2 发送成功

    ;

    private String subject;

    SendStatus(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
