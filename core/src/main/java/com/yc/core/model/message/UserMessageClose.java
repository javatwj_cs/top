package com.yc.core.model.message;

import com.yc.core.model.AbstractEntity;
import com.yc.core.model.user.User;

import javax.persistence.*;

/**
 * 用户关闭消息提醒
 */
@Entity
public class UserMessageClose extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;                  // 用户
    @ManyToOne(fetch = FetchType.LAZY)
    private MessageParamManage messageParamManage;  // 消息参数管理

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public MessageParamManage getMessageParamManage() {
        return messageParamManage;
    }

    public void setMessageParamManage(MessageParamManage messageParamManage) {
        this.messageParamManage = messageParamManage;
    }
}
