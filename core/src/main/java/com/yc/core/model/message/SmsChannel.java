package com.yc.core.model.message;


import com.yc.core.model.AbstractEntity;
import com.yc.core.service.message.sms.SmsSenderType;

import javax.persistence.*;

/**
 * 短信渠道
 */
@Entity
public class SmsChannel extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String name;            // 渠道名称
    private String url;             // 短信发送地址
    private String username;        // 用户名
    private String password;        // 密码
    private Boolean enable;         // 状态：true：启用 false：禁用
    private Integer sortIndex;      // 排序

    @Enumerated(EnumType.STRING)
    private SmsSenderType smsSenderType;  // 渠道类型

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public SmsSenderType getSmsSenderType() {
        return smsSenderType;
    }

    public void setSmsSenderType(SmsSenderType smsSenderType) {
        this.smsSenderType = smsSenderType;
    }

    public Integer getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(Integer sortIndex) {
        this.sortIndex = sortIndex;
    }
}
