package com.yc.core.model.message;


import com.yc.core.model.AbstractEntity;
import com.yc.core.model.admin.AdminUser;
import com.yc.core.model.user.User;
import com.yc.core.service.message.MessageAction;

import javax.persistence.*;

/**
 * 站内消息，站内信
 */
@Entity
public class StationLetter extends AbstractEntity {


    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;                  // 用户
    private String content;             // 内容
    private MessageAction messageAction;// 消息类型
    @ManyToOne(fetch = FetchType.LAZY)
    private AdminUser sender;           // 发送者编号：0，系统 >0，管理员
    private String senderName;          // 发送者名称：system 或 管理员名称

    private Boolean status;             // 状态：false,未读 true,已读


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public AdminUser getSender() {
        return sender;
    }

    public void setSender(AdminUser sender) {
        this.sender = sender;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public MessageAction getMessageAction() {
        return messageAction;
    }

    public void setMessageAction(MessageAction messageAction) {
        this.messageAction = messageAction;
    }

}
