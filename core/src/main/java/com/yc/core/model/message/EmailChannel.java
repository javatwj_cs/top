package com.yc.core.model.message;

import com.yc.core.model.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 邮箱渠道
 */
@Entity
public class EmailChannel extends AbstractEntity {


    @Id
    @GeneratedValue
    private Long id;

    private String name;            // 渠道名称

    private String host;            // 邮箱host
    private String protocol;        // 邮箱协议
    private String username;        // 用户名
    private String password;        // 密码

    private Boolean enable;         // 状态：true：启用 false：禁用

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}
