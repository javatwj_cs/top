package com.yc.core.model.message;


import com.yc.core.model.AbstractEntity;
import com.yc.core.service.message.MessageAction;
import com.yc.core.service.message.MessageChannel;

import javax.persistence.*;

/**
 * 系统消息参数
 */
@Entity
public class MessageParamManage extends AbstractEntity {
    @Id
    @GeneratedValue
    private Long id;
    @Enumerated(EnumType.STRING)
    private MessageAction messageAction;    // 消息动作
    @Enumerated(EnumType.STRING)
    private MessageChannel messageChannel;  // 消息渠道
    private String content;                 // 内容
    private Boolean userStatus;             // 是否允许用户关闭 true允许 false不允许
    private Boolean enable;                 // 是否启用
    private String remark;                  // 备注

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MessageAction getMessageAction() {
        return messageAction;
    }

    public void setMessageAction(MessageAction messageAction) {
        this.messageAction = messageAction;
    }

    public MessageChannel getMessageChannel() {
        return messageChannel;
    }

    public void setMessageChannel(MessageChannel messageChannel) {
        this.messageChannel = messageChannel;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Boolean userStatus) {
        this.userStatus = userStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}
