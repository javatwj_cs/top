package com.yc.core.model.user;

import com.yc.common.constant.Terminal;
import com.yc.core.model.AbstractEntity;
import com.yc.core.model.account.Account;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by tianjie on 17/7/16.
 * 用户基本信息
 */
@Entity
public class User extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String username;        // 用户名
    private String mobile;          // 手机
    private String email;           // 邮箱
    private String wxToken;         // 微信

    private String passwordLogin;   // 登陆密码
    private Boolean enable;         // 是否启用 true：启用 false：禁用

    private String name;            // 真实姓名
    private String idCard;          // 用户身份证
    private String nickName;        // 昵称
    private String img;             // 头像
    private Boolean gender;         // 性别 true:男 false：女
    private Long birthday;          // 生日
    private Integer marriage;       // 1:未婚 2:已婚 3:保密
    private Long province;          // 省
    private Long city;              // 市
    private Long area;              // 区


    @ManyToOne(fetch = FetchType.LAZY)
    private User recommend;

    private Terminal terminal;      // 注册终端

    private String loginLastIp;     // 最后登陆IP
    private Date loginLastTime;     // 最后登陆时间

    @OneToOne(fetch = FetchType.LAZY)
    private Account account;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWxToken() {
        return wxToken;
    }

    public void setWxToken(String wxToken) {
        this.wxToken = wxToken;
    }

    public String getPasswordLogin() {
        return passwordLogin;
    }

    public void setPasswordLogin(String passwordLogin) {
        this.passwordLogin = passwordLogin;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public Long getBirthday() {
        return birthday;
    }

    public void setBirthday(Long birthday) {
        this.birthday = birthday;
    }

    public Integer getMarriage() {
        return marriage;
    }

    public void setMarriage(Integer marriage) {
        this.marriage = marriage;
    }

    public Long getProvince() {
        return province;
    }

    public void setProvince(Long province) {
        this.province = province;
    }

    public Long getCity() {
        return city;
    }

    public void setCity(Long city) {
        this.city = city;
    }

    public Long getArea() {
        return area;
    }

    public void setArea(Long area) {
        this.area = area;
    }

    public User getRecommend() {
        return recommend;
    }

    public void setRecommend(User recommend) {
        this.recommend = recommend;
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public String getLoginLastIp() {
        return loginLastIp;
    }

    public void setLoginLastIp(String loginLastIp) {
        this.loginLastIp = loginLastIp;
    }

    public Date getLoginLastTime() {
        return loginLastTime;
    }

    public void setLoginLastTime(Date loginLastTime) {
        this.loginLastTime = loginLastTime;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
