package com.yc.core.model.message;


import com.yc.core.model.AbstractEntity;
import com.yc.core.model.admin.AdminUser;
import com.yc.core.model.user.User;
import com.yc.core.service.message.MessageAction;
import com.yc.core.service.message.SendStatus;

import javax.persistence.*;

/**
 * 邮箱发送日志
 */
@Entity
public class EmailLog extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;                  // 用户：可为空
    private String email;               // 邮件地址
    private String subject;             // 邮件主题
    @Column(columnDefinition="TEXT")
    private String content;             // 发送内容
    @Enumerated(EnumType.STRING)
    private MessageAction messageAction;// 消息类型
    @ManyToOne(fetch = FetchType.LAZY)
    private EmailChannel channel;       // 邮箱渠道
    private SendStatus sendStatus;          // 发送状态
    @Column(columnDefinition="TEXT")
    private String returnMessage;       // 返回消息：发送失败时记录错误消息，发送成功时可为空

    @ManyToOne(fetch = FetchType.LAZY)
    private AdminUser sender;           // 发送者编号：0，系统 >0，管理员
    private String senderName;          // 发送者名称：system 或 管理员名称

//    private Date senderDate;          // 短信发送时间，如果短信是即时发送，可以不需要。如果短信在系统有逗留时间，需要记录发送时间


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public EmailChannel getChannel() {
        return channel;
    }

    public void setChannel(EmailChannel channel) {
        this.channel = channel;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public AdminUser getSender() {
        return sender;
    }

    public void setSender(AdminUser sender) {
        this.sender = sender;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public MessageAction getMessageAction() {
        return messageAction;
    }

    public void setMessageAction(MessageAction messageAction) {
        this.messageAction = messageAction;
    }

    public SendStatus getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(SendStatus sendStatus) {
        this.sendStatus = sendStatus;
    }

}
