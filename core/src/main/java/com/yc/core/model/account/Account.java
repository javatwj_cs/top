package com.yc.core.model.account;

import com.yc.common.util.MD5Util;
import com.yc.core.model.AbstractEntity;
import com.yc.core.model.user.User;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by tianjie on 17/8/20.
 */
@Entity
public class Account extends AbstractEntity {

    public static final String ACCOUNT_PRIVATE_KEY = "'aF8'g!qhp0l-xed[wBn|L]+W!bU<b5K$U:CaykCsrI^\\ro[Y;;>gjB(_uTg~$fOoTW^l!jDcc0encg(*kJAE'xh;M,t[q@>)sFs)[2o|l!`Z-2yfSIWvHm.RQhSaF[P";

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    private User user;
    private BigDecimal amount;
    private BigDecimal freezeAmount;

    private Integer integral;

    private String sign;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getFreezeAmount() {
        return freezeAmount;
    }

    public void setFreezeAmount(BigDecimal freezeAmount) {
        this.freezeAmount = freezeAmount;
    }

    public Integer getIntegral() {
        return integral;
    }

    public void setIntegral(Integer integral) {
        this.integral = integral;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String generateSign() {
        StringBuffer sb = new StringBuffer();

        sb.append(ACCOUNT_PRIVATE_KEY);
        sb.append("amount:").append(amount);
        sb.append("freezeAmount:").append(freezeAmount);
        sb.append("integral:").append(integral);

        return MD5Util.md5AsHex(sb.toString());
    }
}
