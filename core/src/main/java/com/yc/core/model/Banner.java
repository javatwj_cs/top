package com.yc.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.yc.common.constant.BannerPlace;
import com.yc.common.constant.BannerContentType;
import com.yc.common.constant.Terminal;

/**
 *
 */
@Entity
public class Banner extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String title;
    private String content;
    private String image;

    @Enumerated(EnumType.STRING)
    private BannerContentType contentType;  // 类型
    @Enumerated(EnumType.STRING)
    private BannerPlace place;              // 位置（所属栏目）
    private Terminal terminal;              // 终端
    private Boolean enable;                 // 是否启用
    private Integer shutTimes;              // 点击次数
    private Integer sortIndex;              // 序号

    private String operatorName;            // 操作员

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public BannerContentType getContentType() {
        return contentType;
    }

    public void setContentType(BannerContentType contentType) {
        this.contentType = contentType;
    }

    public BannerPlace getPlace() {
        return place;
    }

    public void setPlace(BannerPlace place) {
        this.place = place;
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public Integer getShutTimes() {
        return shutTimes;
    }

    public void setShutTimes(Integer shutTimes) {
        this.shutTimes = shutTimes;
    }

    public Integer getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(Integer sortIndex) {
        this.sortIndex = sortIndex;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }
}
