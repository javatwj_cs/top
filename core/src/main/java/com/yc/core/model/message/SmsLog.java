package com.yc.core.model.message;


import com.yc.core.model.AbstractEntity;
import com.yc.core.model.admin.AdminUser;
import com.yc.core.model.user.User;
import com.yc.core.service.message.MessageAction;
import com.yc.core.service.message.SendStatus;

import javax.persistence.*;

/**
 * 短信发送日志
 */
@Entity
public class SmsLog extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;              // 用户：可为空
    private String mobile;          // 手机号
    private String content;         // 发送内容
    @ManyToOne(fetch = FetchType.LAZY)
    private SmsChannel channel;     // 短信渠道
    @Enumerated(EnumType.STRING)
    private SendStatus sendStatus;      // 发送状态：1，发送中 2，发送失败 3，发送成功
    @Enumerated(EnumType.STRING)
    private MessageAction messageAction;// 消息类型
    @ManyToOne(fetch = FetchType.LAZY)
    private AdminUser sender;       // 发送者编号：0，系统 >0，管理员
    private String senderName;          // 发送者名称：system 或 管理员名称
    @Column(columnDefinition="TEXT")
    private String returnMessage;   // 返回消息：发送失败时记录错误消息，发送成功时可为空

//    @Column(name = "user_id", insertable = false, updatable = false)
//    private Long userId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public SmsChannel getChannel() {
        return channel;
    }

    public void setChannel(SmsChannel channel) {
        this.channel = channel;
    }

    public AdminUser getSender() {
        return sender;
    }

    public void setSender(AdminUser sender) {
        this.sender = sender;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public MessageAction getMessageAction() {
        return messageAction;
    }

    public void setMessageAction(MessageAction messageAction) {
        this.messageAction = messageAction;
    }

    public SendStatus getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(SendStatus sendStatus) {
        this.sendStatus = sendStatus;
    }
}
