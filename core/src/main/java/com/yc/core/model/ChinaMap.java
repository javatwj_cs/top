package com.yc.core.model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * 中国城市
 */
@Entity
public class ChinaMap extends AbstractEntity {

    // province_id（空） city_id（空）     省
    // province_id（非空） city_id（空）   市
    // province_id（非空） city_id（非空） 区/县

    @Id
    private Long id;


    private String code;        // 代码
    private String name;        // 名称

    private String provinceId; // 省
    private String cityId;     // 市

//    // code 非唯一，需要通过province_id 和 city_id进行匹配，所以这里不进行关联

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(referencedColumnName = "code", name = "province_id")
//    private ChinaMap province;  // 省份
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(referencedColumnName = "code", name = "city_id")
//    private ChinaMap city;      // 城市


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }
}
