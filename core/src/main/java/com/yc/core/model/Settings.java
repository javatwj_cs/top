package com.yc.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.function.Function;

/**
 * 系统参数
 */
@Entity
public class Settings extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true, nullable = false)
    private String name;
    @Column(columnDefinition = "text")
    private String value;
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public <T> T getMappedValue(Function<String, T> function) {
        String v = getValue();
        return v != null ? function.apply(v) : null;
    }

    public Boolean getBooleanValue() {
        return getMappedValue(Boolean::valueOf);
    }

}
