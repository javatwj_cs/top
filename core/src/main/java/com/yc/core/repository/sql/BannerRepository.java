package com.yc.core.repository.sql;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;

import com.yc.common.constant.BannerPlace;
import com.yc.common.constant.Terminal;
import com.yc.core.model.message.SmsLog;
import com.yc.core.service.message.MessageAction;
import com.yc.core.service.message.SendStatus;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.yc.core.model.Banner;

@Repository
public interface BannerRepository extends JpaRepository<Banner, Long>, JpaSpecificationExecutor<Banner>, EntityManagerRepository<Banner, Long> {

    /**
     * 分页查询banner
     * @param enable
     * @param title
     * @param place
     * @param terminal
     * @param pageable
     * @return
     */
    default Page<Banner> findPage(String title, BannerPlace place, Terminal terminal, Boolean enable, Pageable pageable) {
        return findAll((root, query, cb) -> {
            List<Predicate> list = new ArrayList<>();

            if(enable != null) {
                list.add(cb.equal(root.get("enable"), enable));
            }
            if(StringUtils.isNotBlank(title)) {
                list.add(cb.like(root.get("title"), title + "%"));
            }
            if(place != null) {
                list.add(cb.equal(root.get("place"), place));
            }
            if(terminal != null) {
                list.add(cb.equal(root.get("terminal"), terminal));
            }

            query.where(list.toArray(new Predicate[list.size()]));
            query.orderBy(cb.desc(root.get("id")));

            return null;
        }, pageable);
    }

    /**
     * 根据位置和终端，查询启动的banner
     * @param place
     * @param terminal
     * @return
     */
    List<Banner> getByPlaceAndTerminalAndEnableTrue(BannerPlace place, Terminal terminal);


}
