package com.yc.core.repository.sql.account;

import com.yc.core.model.account.AccountBill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountBillRepository extends JpaRepository<AccountBill, Long> {

}
