package com.yc.core.repository.sql;

import com.yc.core.model.Settings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SettingsRepository extends JpaRepository<Settings, Long> {

    Settings findByName(String name);

    List<Settings> findByNameStartingWith(String name);
}
