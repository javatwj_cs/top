package com.yc.core.repository.sql.message;

import com.yc.core.model.message.SmsChannel;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SmsChannelRepository extends JpaRepository<SmsChannel, Long> {

    List<SmsChannel> findByEnableTrue(Sort sort);

}
