package com.yc.core.repository.sql;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * 扩展Spring默认Respository接口。
 *
 * @param <T> 实体类型
 * @param <ID> ID类型
 */
@NoRepositoryBean
public interface EntityManagerRepository<T, ID extends Serializable> extends PagingAndSortingRepository<T, ID> {

    /**
     * 获取EntityManager实例。
     *
     * @return 实例
     */
    EntityManager getEntityManager();

}
