package com.yc.core.repository.sql;

import com.yc.core.model.ChinaMap;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public interface ChinaMapRepository extends JpaSpecificationExecutor<ChinaMap>, EntityManagerRepository<ChinaMap, Long> {

    default List<ChinaMap> findAll(String provinceId, String cityId) {
        return findAll((root, query, cb) -> {
            List<Predicate> list = new ArrayList<>();

            if(provinceId == null ){
                list.add(cb.isNull(root.get("provinceId")));
            }else{
                list.add(cb.equal(root.get("provinceId"), provinceId));
            }

            if(cityId == null ){
                list.add(cb.isNull(root.get("cityId")));
            }else{
                list.add(cb.equal(root.get("cityId"), cityId));
            }


            query.where(list.toArray(new Predicate[list.size()]));

            return null;
        });
    }

    default ChinaMap findOne(String code, String provinceId, String cityId) {
        return findOne((root, query, cb) -> {
            List<Predicate> list = new ArrayList<>();

            list.add(cb.equal(root.get("code"), code));

            if(provinceId == null ){
                list.add(cb.isNull(root.get("provinceId")));
            }else{
                list.add(cb.equal(root.get("provinceId"), provinceId));
            }

            if(cityId == null ){
                list.add(cb.isNull(root.get("cityId")));
            }else{
                list.add(cb.equal(root.get("cityId"), cityId));
            }


            query.where(list.toArray(new Predicate[list.size()]));

            return null;
        });
    }

}
