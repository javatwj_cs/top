package com.yc.core.repository.sql.message;

import com.yc.core.model.message.EmailChannel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailChannelRepository extends JpaRepository<EmailChannel, Long> {

}
