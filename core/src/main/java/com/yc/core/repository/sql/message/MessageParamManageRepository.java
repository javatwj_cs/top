package com.yc.core.repository.sql.message;

import com.yc.core.model.message.MessageParamManage;
import com.yc.core.service.message.MessageAction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageParamManageRepository extends JpaRepository<MessageParamManage, Long> {

    @Query("from MessageParamManage u where u.messageAction = ?1 and enable = true ")
    List<MessageParamManage> findByMessageAction(MessageAction messageAction);

}
