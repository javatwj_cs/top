package com.yc.core.repository.sql.user;

import com.yc.core.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tianjie on 17/7/16.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    public User findByUsername(String username);
    public User findByMobile(String mobile);
    public User findByEmail(String email);
    public User findByWxToken(String wxToken);

}
