package com.yc.core.repository.sql.message;

import com.yc.core.model.message.UserMessageClose;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMessageCloseRepository extends JpaRepository<UserMessageClose, Long> {

    List<UserMessageClose> findByUserId(Long userId);
}
