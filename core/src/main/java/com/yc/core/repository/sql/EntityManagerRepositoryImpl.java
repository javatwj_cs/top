package com.yc.core.repository.sql;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * 实现自定义扩展接口。
 *
 * @param <T> 实体类型
 * @param <ID> ID类型
 */
public class EntityManagerRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements EntityManagerRepository<T, ID> {

    private final EntityManager entityManager;

    public EntityManagerRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

}
