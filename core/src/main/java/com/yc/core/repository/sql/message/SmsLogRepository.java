package com.yc.core.repository.sql.message;

import com.yc.core.model.message.SmsLog;
import com.yc.core.repository.sql.EntityManagerRepository;
import com.yc.core.service.message.MessageAction;
import com.yc.core.service.message.SendStatus;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public interface SmsLogRepository extends JpaRepository<SmsLog, Long>, JpaSpecificationExecutor<SmsLog>, EntityManagerRepository<SmsLog, Long> {


    default Page<SmsLog> findPage(Date startDate, Date endDate, String username, String serviceName, MessageAction messageAction, SendStatus sendStatus, Pageable pageable) {
        return findAll((root, query, cb) -> {
            List<Predicate> list = new ArrayList<>();

            if(startDate != null ){
                list.add(cb.greaterThanOrEqualTo(root.get("createTime"), startDate));
            }
            if(endDate != null) {
                list.add(cb.lessThanOrEqualTo(root.get("createTime"), endDate));
            }
            if(StringUtils.isNotBlank(username)) {
                list.add(cb.like(root.get("user").get("username"), username + "%"));
            }
            if (StringUtils.isNotBlank(serviceName)) {
                list.add(cb.like(root.get("user").get("service").get("username"), serviceName + "%"));
            }
            if(messageAction != null) {
                list.add(cb.equal(root.get("messageAction"), messageAction));
            }
            if(sendStatus != null) {
                list.add(cb.equal(root.get("sendStatus"), sendStatus));
            }

            if(query.getResultType() != Long.class) {
                root.fetch("user", JoinType.LEFT).fetch("service", JoinType.LEFT);       // 获取懒加载数据
            }

            query.where(list.toArray(new Predicate[list.size()]));
            query.orderBy(cb.desc(root.get("id")));

            return null;
        }, pageable);
    }

}
