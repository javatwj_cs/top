package com.yc.core.cache;

import com.alibaba.fastjson.JSON;
import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import com.yc.common.util.Verification;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;


@Component
public class MobileTokenCacheImpl implements MobileTokenCache {
	
	public static final int REDIS_MOBILE_CODE_INDEX = 1;
	public static final String REDIS_IMG_CODE_HEADER = "MOBILE_TOKEN_";

	public static final int REDIS_MOBILE_ERROR_NUM = 5;					// 允许错误次数
	public static final int REDIS_MOBILE_TOKEN_ACTIVE = 5;				// 有效时间（单位：分钟）

	@Autowired
	private RedisTemplate<String, String> redisTemplate;


	private String getKey(String key) {
		return REDIS_IMG_CODE_HEADER + key;
	}

	private void setMobileCode(String key, MobileToken mobileToken) {
		redisTemplate.opsForValue().set(getKey(key), JSON.toJSONString(mobileToken), REDIS_MOBILE_TOKEN_ACTIVE, TimeUnit.MINUTES);
	}


	private void delMobileCode(String key) {
		redisTemplate.delete(getKey(key));
	}


	@Override
	public MobileToken set(String mobile) {
		MobileToken mobileToken = get(mobile);

		// 检验是否禁用
		if(mobileToken != null && mobileToken.checkErrorNum()) {
			throw new ServiceException(ReturnCode.CODE_40001.getCode(), "禁用" + MobileTokenCacheImpl.REDIS_MOBILE_TOKEN_ACTIVE + "天");
		}

		// 是否允许重新发送
		if(mobileToken != null && !mobileToken.checkResendTime()) {
			return mobileToken;
		}

		mobileToken = new MobileToken(MobileToken.generateToken(), System.currentTimeMillis());

		setMobileCode(mobile, mobileToken);

		return mobileToken;
	}

	@Override
	public MobileToken get(String mobile) {
		String returnValue = redisTemplate.opsForValue().get(getKey(mobile));
		if(StringUtils.isBlank(returnValue)) {
			return null;
		}

		return JSON.parseObject(returnValue, MobileToken.class);
	}
	
	@Override
	public void del(String mobile) {
		delMobileCode(mobile);
	}
	
	@Override
	public boolean checkToken(String mobile, String token) {
		if(!Verification.isValidMobile(mobile)) {
			throw new ServiceException(ReturnCode.CODE_40004, "手机号码");
		}
		if(StringUtils.isBlank(token) || token.length() != MobileToken.MOBILE_TOKEN_LENGTH) {
			throw new ServiceException(ReturnCode.CODE_40004, "手机验证码");
		}

		MobileToken mobileToken = get(mobile);

		// 是否禁用
		if(mobileToken.checkErrorNum()) {
			throw new ServiceException(ReturnCode.CODE_40001.getCode(), "禁用" + MobileTokenCacheImpl.REDIS_MOBILE_TOKEN_ACTIVE + "天");
		}

		// 是否有效
		if(!mobileToken.checkValidTime()) {
			throw new ServiceException(ReturnCode.CODE_50001.getCode(), "已过有效期");
		}
		
		// 验证准确性
		if(token.equals(mobileToken.getToken())) {
			return true;
		}

		// 累加错误次数，达到5次，删除验证码
		mobileToken.setErrorNum(mobileToken.getErrorNum() + 1);

		setMobileCode(mobile, mobileToken);

		return false;
	}
}
