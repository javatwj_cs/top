package com.yc.core.cache;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.Serializable;

public class ImgToken implements Serializable {
	
	private static final long serialVersionUID = 5810304820532881439L;

	public static final int REDIS_IMG_CODE_COUNT = 4;	// 图片验证码位数
	
	/** 有效期 */
	public static long VALIDITY_TIME = 1800000;
	
	/** 验证码 */
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public static String generateToken() {
		return RandomStringUtils.randomAlphanumeric(REDIS_IMG_CODE_COUNT);
	}
}
