package com.yc.core.cache;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.Serializable;

public class MobileToken implements Serializable {
	
	private static final long serialVersionUID = 5810304820532881439L;
	
	/** 结束时间差：十分钟 */
	public static long END_TIME_INTERVAL = 600000;
	/** 重发时间差：一分钟 */
	public static long RE_TIME_INTERVAL = 60000;
	/** 手机验证码长度：4 */
	public static int MOBILE_TOKEN_LENGTH = 4;


	/** 验证码 */
	private String token;
	
	/** 有效时间：开始时间 */
	private long startTime;
	
	/** 有效时间：结束时间 */
	private long endTime;
	
	/** 重发时间 */
	private long reTime;
	/** 错误次数 */
	private int errorNum;

	public MobileToken() {
	}

	public MobileToken(String token, long startTime) {
		this.setToken(token);
		this.setStartTime(startTime);
		this.setErrorNum(0);
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
		
		this.endTime = startTime + END_TIME_INTERVAL;
		this.reTime = startTime + RE_TIME_INTERVAL;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public long getReTime() {
		return reTime;
	}

	public void setReTime(long reTime) {
		this.reTime = reTime;
	}

	public int getErrorNum() {
		return errorNum;
	}

	public void setErrorNum(int errorNum) {
		this.errorNum = errorNum;
	}

	/**
	 * 验证码验证失败?次，号码禁用?天
	 * @return
	 * true : 已被禁用
	 * false：正常
	 */
	public boolean checkErrorNum() {
		return MobileTokenCacheImpl.REDIS_MOBILE_ERROR_NUM == 0 ? Boolean.FALSE : this.getErrorNum() >= MobileTokenCacheImpl.REDIS_MOBILE_ERROR_NUM;
	}

	/**
	 * 检查是否允许重新发送
	 * @return
	 * true : 允许
	 * false：不允许
	 */
	public boolean checkResendTime() {
		long now = System.currentTimeMillis();

		return this.getReTime() <= now;

	}

	/**
	 * 检查时间是否有效
	 * @return
	 * true	： 有效时间内
	 * false：已过期
	 */
	public boolean checkValidTime() {
		long now = System.currentTimeMillis();

		return this.getStartTime() <= now && this.getEndTime() >= now;
	}


	public static String generateToken() {
		return RandomStringUtils.randomNumeric(MOBILE_TOKEN_LENGTH);
	}
}
