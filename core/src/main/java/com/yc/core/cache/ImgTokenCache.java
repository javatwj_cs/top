package com.yc.core.cache;


/**
 * 图片验证码缓存
 * @author tianjie_cs
 *
 */
public interface ImgTokenCache {

	/**
	 * 缓存图片验证码
	 * @param imgToken
	 * @param imgKey
	 */
	public ImgToken set(String imgKey, String imgToken);

	/**
	 * 缓存图片验证码，
	 * 每次重新生成一个验证码，并进行返回
	 * @param imgKey
	 * @return
	 */
	public ImgToken set(String imgKey);

	/**
	 * 图片验证码
	 * @param imgKey
	 * @return
	 */
	public ImgToken get(String imgKey);

	/**
	 * 删除图片验证码缓存
	 * 清理图片空间
	 * @param imgKey
	 */
	public void del(String imgKey);

	/**
	 * 检验验证码
	 * 验证通过，删除数据
	 * 验证失败，刷新数据
	 * @param imgKey
	 * @param imgToken
	 * @return
	 */
	public boolean checkToken(String imgKey, String imgToken);
}
