package com.yc.core.cache;

import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;

import java.util.concurrent.TimeUnit;


@Component
public class ImgTokenCacheImpl implements ImgTokenCache {
	
	private final int REDIS_IMG_CODE_INDEX = 1;
	private final String REDIS_IMG_CODE_HEADER = "IMG_CODE_";

	public static final int REDIS_IMG_CODE_ACTIVE = 5;	// 有效时间（单位：分钟）

	@Autowired
	private RedisTemplate<String, String> redisTemplate;


	private String getKey(String key) {
		return REDIS_IMG_CODE_HEADER + key;
	}

	private void setImgCode(String key, ImgToken imgToken) {
		redisTemplate.opsForValue().set(getKey(key), JSON.toJSONString(imgToken), REDIS_IMG_CODE_ACTIVE, TimeUnit.MINUTES);
	}


	@Override
	public ImgToken set(String imgKey, String token) {
		ImgToken imgToken = new ImgToken();
		
		imgToken.setToken(token);
		
		setImgCode(imgKey, imgToken);

		return imgToken;
	}

	@Override
	public ImgToken set(String imgKey) {
		String token = ImgToken.generateToken();

		return set(imgKey, token);
	}

	@Override
	public ImgToken get(String imgKey) {
		String returnValue = redisTemplate.opsForValue().get(getKey(imgKey));
		if(StringUtils.isBlank(returnValue)) {
			return null;
		}

		return JSON.parseObject(returnValue, ImgToken.class);
	}

	@Override
	public void del(String imgKey) {
		redisTemplate.delete(getKey(imgKey));
	}

	@Override
	public boolean checkToken(String imgKey, String token) {
		if(StringUtils.isBlank(token) || token.length() != ImgToken.REDIS_IMG_CODE_COUNT) {
			throw new ServiceException(ReturnCode.CODE_40004, "图片验证码");
		}

		ImgToken imgCode = get(imgKey);
		
		if(imgCode == null || StringUtils.isBlank(imgCode.getToken())) {
			throw new ServiceException(ReturnCode.CODE_50001, "验证码已失效");
		}

		if(imgCode.getToken().equalsIgnoreCase(token)) {
			this.del(imgKey);

			return true;
		}

		return false;
	}

}
