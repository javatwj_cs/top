package com.yc.core.cache;


/**
 * 手机验证码缓存
 * @author tianjie_cs
 *
 */
public interface MobileTokenCache {

	/**
	 * 缓存手机验证码
	 * @param mobile
	 *
	 */
	public MobileToken set(String mobile);
	
	/**
	 * 获取手机验证码
	 * @param mobile
	 * @return
	 */
	public MobileToken get(String mobile);
	
	/**
	 * 删除手机验证码
	 * @param mobile
	 */
	public void del(String mobile);
	
	/**
	 * 校验手机验证码
	 * @param mobile
	 * @param token
	 * @return 
	 * true：	验证通过
	 * false：	验证失败
	 */
	public boolean checkToken(String mobile, String token);
}
