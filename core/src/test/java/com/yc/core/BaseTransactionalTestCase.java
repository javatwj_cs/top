package com.yc.core;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

/**
 * 基础测试类，用于集成Spring上下文，并在测试完成后回滚事务。
 *
 */
@SpringBootTest(classes = App.class)
public abstract class BaseTransactionalTestCase extends AbstractTransactionalJUnit4SpringContextTests {

}
