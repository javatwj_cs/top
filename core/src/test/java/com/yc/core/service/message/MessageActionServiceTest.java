package com.yc.core.service.message;

import java.util.ArrayList;
import java.util.List;

import com.yc.core.BaseTestCase;
import com.yc.core.BaseTransactionalTestCase;
import com.yc.core.model.message.EmailChannel;
import com.yc.core.model.message.MessageParamManage;
import com.yc.core.model.message.SmsChannel;
import com.yc.core.repository.sql.message.EmailChannelRepository;
import com.yc.core.repository.sql.message.MessageParamManageRepository;
import com.yc.core.repository.sql.message.SmsChannelRepository;
import com.yc.core.service.message.sms.SmsSender;
import com.yc.core.service.message.sms.SmsSenderType;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

/**
 *
 */
public class MessageActionServiceTest extends BaseTestCase {

    @Autowired
    private EmailChannelRepository emailChannelRepository;
    @Autowired
    private SmsChannelRepository smsChannelRepository;
    @Autowired
    private MessageParamManageRepository messageParamManageRepository;


    @Test
//    @Ignore
    public void initChannel() {
        /** 添加邮件渠道 */
        EmailChannel emailChannel = new EmailChannel();
        emailChannel.setName("主邮箱");
        emailChannel.setHost("smtp.163.com");
        emailChannel.setProtocol("smtp");
        emailChannel.setUsername("emailName_d1f5@email.xxx");
        emailChannel.setPassword("");
        emailChannel.setEnable(Boolean.TRUE);

        emailChannelRepository.save(emailChannel);

        /** 添加短信渠道 */
        SmsChannel smsChannel = new SmsChannel();
        smsChannel.setName("simpleSms");
        smsChannel.setUrl("http://example");
        smsChannel.setUsername("username");
        smsChannel.setPassword("123456");
        smsChannel.setEnable(Boolean.TRUE);
        smsChannel.setSmsSenderType(SmsSenderType.SIMPLE);
        smsChannel.setSortIndex(0);

        smsChannelRepository.save(smsChannel);
    }

    @Test
//    @Ignore
    public void initMessageParamManage() {
        List<MessageParamManage> messageParamManageList = new ArrayList<>();
        MessageParamManage messageParamManage;

        // 手机注册 - 手机
        messageParamManage = new MessageParamManage();

        messageParamManage.setMessageAction(MessageAction.REGISTER_MOBILE);
        messageParamManage.setMessageChannel(MessageChannel.SMS);
        messageParamManage.setContent("验证码：${mobileToken}（账号注册动态码，请勿泄露，5分钟内有效）");
        messageParamManage.setUserStatus(Boolean.FALSE);
        messageParamManage.setEnable(Boolean.TRUE);

        messageParamManageList.add(messageParamManage);

        // 绑定微信 - 手机
        messageParamManage = new MessageParamManage();

        messageParamManage.setMessageAction(MessageAction.BIND_WX);
        messageParamManage.setMessageChannel(MessageChannel.SMS);
        messageParamManage.setContent("验证码：${mobileToken}（绑定微信动态码，请勿泄露，5分钟内有效）");
        messageParamManage.setUserStatus(Boolean.FALSE);
        messageParamManage.setEnable(Boolean.TRUE);

        messageParamManageList.add(messageParamManage);

//        // 注册成功 - 手机
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.REGISTER_SUCCESS);
//        messageParamManage.setMessageChannel(MessageChannel.SMS);
//        messageParamManage.setContent("尊敬的${username}，欢迎您加入澳门银河，官方平台资金无忧，新注册会员尊享优惠，首存大礼送不停，详情查询官网最新优惠。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 注册成功 - 站内信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.REGISTER_SUCCESS);
//        messageParamManage.setMessageChannel(MessageChannel.STATION_LETTER);
//        messageParamManage.setContent("尊敬的${username}，欢迎您加入澳门银河，官方平台资金无忧，新注册会员尊享优惠，首存大礼送不停，详情查询官网最新优惠。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 存款成功(线下) - 短信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.DEPOSIT_OFFLINE_SUCCESS);
//        messageParamManage.setMessageChannel(MessageChannel.SMS);
//        messageParamManage.setContent("您已成功充值${depositAmount}，账户余额为${balance}");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 提现申请 - 短信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.WITHDRAW_APPLY);
//        messageParamManage.setMessageChannel(MessageChannel.SMS);
//        messageParamManage.setContent("您于${withdrawApplyDate?string('yyyy-MM-dd HH:mm:ss')}提交的提现申请，提现金额为${withdrawAmount}，如非本人操作请联系官方客服。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 提现成功 - 短信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.WITHDRAW_SUCCESS);
//        messageParamManage.setMessageChannel(MessageChannel.SMS);
//        messageParamManage.setContent("您于${withdrawApplyDate?string('yyyy-MM-dd HH:mm:ss')}提交的提现申请已成功提现${withdrawAmount}，请及时查收到账银行确认到账！");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 优惠获取成功 - 短信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.FAVORABLE_SUCCESS);
//        messageParamManage.setMessageChannel(MessageChannel.SMS);
//        messageParamManage.setContent("尊敬的${username}，你于${favorableApplyDate?string('yyyy-MM-dd HH:mm:ss')}申请的${favorableName}符合我们的要求，现发放奖励信息至您的账户，您可登录平台进行查看，若有疑问还请联系客服。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 优惠获取成功 - 站内信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.FAVORABLE_SUCCESS);
//        messageParamManage.setMessageChannel(MessageChannel.STATION_LETTER);
//        messageParamManage.setContent("尊敬的${username}，你于${favorableApplyDate?string('yyyy-MM-dd HH:mm:ss')}申请的${favorableName}符合我们的要求，现发放奖励信息至您的账户，您可登录平台进行查看，若有疑问还请联系客服。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 洗码到账 - 短信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.REBATE_SUCCESS);
//        messageParamManage.setMessageChannel(MessageChannel.SMS);
//        messageParamManage.setContent("尊敬的${username}，你的账户新到账返利金额为${rebateAmount}，还请及时查收账户。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 洗码到账 - 站内信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.REBATE_SUCCESS);
//        messageParamManage.setMessageChannel(MessageChannel.STATION_LETTER);
//        messageParamManage.setContent("尊敬的${username}，你的账户新到账返利金额为${rebateAmount}，还请及时查收账户。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 佣金到账 - 短信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.COMMISSION_SUCCESS);
//        messageParamManage.setMessageChannel(MessageChannel.SMS);
//        messageParamManage.setContent("尊敬的${username}，你的账户新到账${commissionCycle}佣金，金额为${commissionAmount}，还请及时查收账户。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 佣金到账 - 站内信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.COMMISSION_SUCCESS);
//        messageParamManage.setMessageChannel(MessageChannel.STATION_LETTER);
//        messageParamManage.setContent("尊敬的${username}，你的账户新到账${commissionCycle}佣金，金额为${commissionAmount}，还请及时查收账户。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 手机找回密码 - 短信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.FIND_PASSWORD_MOBILE);
//        messageParamManage.setMessageChannel(MessageChannel.SMS);
//        messageParamManage.setContent("验证码：${mobileToken}（找回密码动态码，请勿泄露，5分钟内有效，如非本人操作请联系官方客服。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 留言回复 - 短信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.QUESTION_REPLY);
//        messageParamManage.setMessageChannel(MessageChannel.SMS);
//        messageParamManage.setContent("尊敬的${username}，您于${questionDate?string('yyyy-MM-dd HH:mm:ss')}的留言管理员已进行回复，还请登录账号及时查看；");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 代理人审核通过 - 短信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.AGENT_APPLY_SUCCESS);
//        messageParamManage.setMessageChannel(MessageChannel.SMS);
//        messageParamManage.setContent("尊敬的${username}，恭喜您通过我们的审核成为代理人，如需帮助可致电400-8888-888，祝您生活愉快！");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 代理人审核通过 - 站内信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.AGENT_APPLY_SUCCESS);
//        messageParamManage.setMessageChannel(MessageChannel.STATION_LETTER);
//        messageParamManage.setContent("尊敬的${username}，恭喜您通过我们的审核成为代理人，如需帮助可致电400-8888-888，祝您生活愉快！");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 会员等级变更 - 短信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.USER_VIP_UPDATE);
//        messageParamManage.setMessageChannel(MessageChannel.SMS);
//        messageParamManage.setContent("尊敬的${username}，恭喜您升级为${vipName}，关于会员权益，请登录平台进行了解。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 会员等级变更 - 站内信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.USER_VIP_UPDATE);
//        messageParamManage.setMessageChannel(MessageChannel.STATION_LETTER);
//        messageParamManage.setContent("尊敬的${username}，恭喜您升级为${vipName}，关于会员权益，请点击此处进行查看。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 邮件找回密码 - 邮件
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.FIND_PASSWORD_EMAIL);
//        messageParamManage.setMessageChannel(MessageChannel.EMAIL);
//        messageParamManage.setContent(
//                "尊敬的${username} ，您好！\n" + "为确保是您本人操作，您已选择通过该邮件地址获取验证码验证身份。请在邮件验证码输入框输入下方验证码：\n" + "${emailToken}\n" + "勿向任何人泄露您收到的验证码。验证码会在邮件发送30分钟后失效。\n" + "为保证您的帐号安全，重置密码成功后，一段时间内不能修改帐号和销户。\n" + "此致\n" + "澳门银河");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 绑定邮箱 - 邮件
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.BIND_EMAIL);
//        messageParamManage.setMessageChannel(MessageChannel.EMAIL);
//        messageParamManage.setContent(
//                "尊敬的${username} ，您好！\n" + "为确保是您本人操作，您已选择通过该邮件地址获取验证码验证身份。请点击以下链接进行激活：\n" + "${emailActivatingUrl}\n" + "（如果您无法点击这个链接，请将此链接复制到浏览器地址栏后访问）。\n" + "为了保障您账号的安全性，请在12小时内完成激活，此链接将在将在您激活过一次后失效。\n" + "注：这是系统发送的邮件，请勿回复。如需帮助可致电400-8888-888，\n" + "此致\n" + "澳门银河");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 手机找回账户密码 - 短信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.FIND_ACCOUNT_PASSWORD_MOBILE);
//        messageParamManage.setMessageChannel(MessageChannel.SMS);
//        messageParamManage.setContent("验证码：${mobileToken}（找回交易密码动态码，请勿泄露，5分钟内有效，如非本人操作请联系官方客服。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 邮件找回账户密码 - 邮件
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.FIND_ACCOUNT_PASSWORD_EMAIL);
//        messageParamManage.setMessageChannel(MessageChannel.EMAIL);
//        messageParamManage
//                .setContent("尊敬的${username} ，您好！\n" + "请在邮件验证码输入框输入下方验证码：\n" + "${emailToken}\n" + "勿向任何人泄露您收到的验证码。验证码会在邮件发送30分钟后失效。\n" + "为保证您的帐号安全，重置密码成功后，一段时间内不能修改帐号和销户。\n" + "此致\n" + "澳门银河");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);
//
//        // 修改手机号 - 短信
//        messageParamManage = new MessageParamManage();
//
//        messageParamManage.setMessageAction(MessageAction.UPDATE_MOBILE);
//        messageParamManage.setMessageChannel(MessageChannel.SMS);
//        messageParamManage.setContent("验证码：${mobileToken}（修改手机号动态码，请勿泄露，5分钟内有效，如非本人操作请联系官方客服。");
//        messageParamManage.setUserStatus(Boolean.FALSE);
//
//        messageParamManageList.add(messageParamManage);

        messageParamManageRepository.save(messageParamManageList);
    }

}