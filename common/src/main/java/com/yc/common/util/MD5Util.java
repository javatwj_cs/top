package com.yc.common.util;

import org.springframework.util.DigestUtils;

/**
 * MD5
 */
public class MD5Util {


    /**
     * MD5 加密
     * @param bytes byte数组
     * @return
     */
    public static String md5AsHex(byte[] bytes) {
        return DigestUtils.md5DigestAsHex(bytes);
    }

    /**
     * MD5 加密
     * @param str 字符串
     * @return
     */
    public static String md5AsHex(String str) {
        return DigestUtils.md5DigestAsHex(str.getBytes());
    }
}
