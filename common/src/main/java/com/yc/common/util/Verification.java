package com.yc.common.util;

import java.util.regex.Pattern;

/**
 * 验证逻辑相关方法。
 *
 */
public abstract class Verification {

    private static final Pattern USERNAME_PATTERN = Pattern.compile("[0-9a-zA-Z]{6,12}");
    private static final Pattern PASSWORD_PATTERN = Pattern.compile("[0-9a-zA-Z]{6,12}");
    private static final Pattern ADMIN_PASSWORD_PATTERN = Pattern.compile("[0-9a-zA-Z]{6,12}");
    private static final Pattern MOBILE_PATTERN = Pattern.compile("1[34578][0-9]{9}");
    // http://emailregex.com/
    private static final Pattern EMAIL_PATTERN = Pattern.compile(
            "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");

    /**
     * 传入字符串是否不为空。
     *
     * @param value 传入值
     * @return true：字符串非空
     */
    public static boolean isNotEmpty(String value) {
        return value != null && value.length() > 0;
    }

    /**
     * 是否为有效的用户名。
     *
     * @param value 传入值
     * @return true：有效用户名
     */
    public static boolean isValidUsername(String value) {
        return verifyByRegex(USERNAME_PATTERN, value);
    }

    /**
     * 是否为有效的密码。
     *
     * @param value 传入值
     * @return true：有效密码
     */
    public static boolean isValidPassword(String value) {
        return verifyByRegex(PASSWORD_PATTERN, value);
    }

    /**
     * 是否为有效的管理用户密码。
     *
     * @param value 传入值
     * @return true：有效密码
     */
    public static boolean isValidAdminPassword(String value) {
        return verifyByRegex(ADMIN_PASSWORD_PATTERN, value);
    }

    /**
     * 是否为有效的手机号。
     *
     * @param value 传入值
     * @return true：有效手机号
     */
    public static boolean isValidMobile(String value) {
        return verifyByRegex(MOBILE_PATTERN, value);
    }

    /**
     * 是否为有效的邮件地址。
     *
     * @param value 传入值
     * @return true：有效邮件地址
     */
    public static boolean isValidEmail(String value) {
        return verifyByRegex(EMAIL_PATTERN, value);
    }

    /**
     * 是否为有效的真实姓名。
     *
     * @param value 传入值
     * @return true：有效真实姓名
     */
    public static boolean isValidName(String value) {
        return value != null && value.length() > 0 && value.length() <= 40;
    }

    private static boolean verifyByRegex(Pattern pattern, String value) {
        return value != null && pattern.matcher(value).matches();
    }
}
