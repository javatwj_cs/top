package com.yc.common.util;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Kryo.DefaultInstantiatorStrategy;
import org.objenesis.strategy.StdInstantiatorStrategy;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Kryo实例工厂类。
 *
 */
public class KryoFactory implements com.esotericsoftware.kryo.pool.KryoFactory {

    private Collection<Class<?>> classes;

    /**
     * 构造方法。
     */
    public KryoFactory() {
        this(Collections.emptySet());
    }

    /**
     * 构造方法。
     *
     * @param registerClasses 待注册类型列表
     */
    public KryoFactory(Class<?>... registerClasses) {
        this(Stream.of(registerClasses).collect(Collectors.toSet()));
    }

    /**
     * 构造方法。
     *
     * @param registerClasses 待注册类型集合
     */
    public KryoFactory(Set<Class<?>> registerClasses) {
        if (registerClasses != null) {
            classes = registerClasses.stream().sorted(Comparator.comparing(Class::getName)).collect(Collectors.toList());
        } else {
            classes = Collections.emptyList();
        }
    }

    /* (non-Javadoc)
     * @see com.esotericsoftware.kryo.pool.KryoFactory#create()
     */
    @Override
    public Kryo create() {
        Kryo k = new Kryo();
        k.setInstantiatorStrategy(new DefaultInstantiatorStrategy(new StdInstantiatorStrategy()));
        k.register(java.util.Date.class);
        k.register(java.sql.Date.class);
        k.register(ArrayList.class);
        k.register(HashMap.class);
        k.register(HashSet.class);
        classes.forEach(k::register);
        return k;
    }

}
