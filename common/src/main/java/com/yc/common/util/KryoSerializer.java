package com.yc.common.util;

import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.pool.KryoPool;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * Kryo序列化工具类。
 *
 */
public class KryoSerializer {

    private final KryoPool pool;
    private final Queue<Output> buffers = new ConcurrentLinkedDeque<>();

    private int defaultBufferSize = 4 * 1024;
    private int maxBufferSize = 1024 * 1024;

    /**
     * 构造方法。
     *
     * @param factory Kryo工厂
     */
    public KryoSerializer(KryoFactory factory) {
        pool = new KryoPool.Builder(factory).softReferences().build();
    }

    public void setDefaultBufferSize(int defaultBufferSize) {
        this.defaultBufferSize = defaultBufferSize;
    }

    public void setMaxBufferSize(int maxBufferSize) {
        this.maxBufferSize = maxBufferSize;
    }

    /**
     * 获取Output实例，如果没有可用的缓存实例，将返回新Output实例。
     *
     * @return Output实例
     */
    public Output borrowOutput() {
        Output output = buffers.poll();
        if (output != null) {
            return output;
        }
        return new Output(defaultBufferSize, maxBufferSize);
    }

    /**
     * 释放Output实例，并放入缓存中以供下次使用，缓存的实例不会被GC。
     *
     * @param output Output实例
     */
    public void releaseOutput(Output output) {
        if (output.position() > defaultBufferSize) {
            return;
        }
        output.clear();
        buffers.offer(output);
    }

    /**
     * 序列化指定实例。
     *
     * @param obj 实例
     * @return byte数组
     */
    public byte[] serialize(final Object obj) {
        return pool.run(kryo -> {
            Output o = borrowOutput();
            try {
                kryo.writeClassAndObject(o, obj);
                return o.toBytes();
            } finally {
                releaseOutput(o);
            }
        });
    }

    /**
     * 反序列化指定数据。
     *
     * @param in 数据
     * @return 实例
     */
    @SuppressWarnings("unchecked")
    public <T> T deserialize(final byte[] in) {
        if (in == null) {
            return null;
        }
        return (T) pool.run(kryo -> {
            try (Input i = new Input(in)) {
                return kryo.readClassAndObject(i);
            }
        });
    }

    // public <T> T copy(T t) {
    //     return pool.run(kryo -> kryo.copy(t));
    // }
    //
    // public <T> T copyShallow(T t) {
    //     return pool.run(kryo -> kryo.copyShallow(t));
    // }

}
