package com.yc.common.util;

import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

/**
 * 实现Spring的Redis序列化接口，以使用Kryo作为正、反序列化实现。
 *
 */
public class KryoRedisSerializer implements RedisSerializer<Object> {

    private final KryoSerializer kryoSerializer;

    /**
     * 构造方法。
     *
     * @param kryoSerializer Kryo序列化实例
     */
    public KryoRedisSerializer(KryoSerializer kryoSerializer) {
        this.kryoSerializer = kryoSerializer;
    }

    /* (non-Javadoc)
     * @see org.springframework.data.redis.serializer.RedisSerializer#serialize(java.lang.Object)
     */
    @Override
    public byte[] serialize(Object t) throws SerializationException {
        return kryoSerializer.serialize(t);
    }

    /* (non-Javadoc)
     * @see org.springframework.data.redis.serializer.RedisSerializer#deserialize(byte[])
     */
    @Override
    public Object deserialize(byte[] bytes) throws SerializationException {
        return kryoSerializer.deserialize(bytes);
    }

}
