package com.yc.common.util;

import com.yc.common.constant.Terminal;
import eu.bitwalker.useragentutils.DeviceType;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RequestUtil {

    public static HttpServletRequest getRequest() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return request;
    }

    public static HttpSession getSession() {
        HttpServletRequest request = getRequest();
        return request.getSession();
    }

    public static ServletContext getServletContext() {
        return getSession().getServletContext();
    }

    public static String getDomain(HttpServletRequest request) {
        String domain = request.getHeader(HttpHeaders.ORIGIN);
        if (domain == null) {
            domain = request.getHeader(HttpHeaders.REFERER);
        }
        return domain;
    }

    public static String getAddress() {
        return getAddress(getRequest());
    }

    public static String getAddress(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (isInvalidIp(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (isInvalidIp(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (isInvalidIp(ip)) {
            ip = request.getRemoteAddr();
        }
        return "0:0:0:0:0:0:0:1".equals(ip) ? "127.0.0.1" : ip;
    }

    private static boolean isInvalidIp(String ip) {
        return ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip);
    }

    /**
     * 确定终端，返回值可能为null。
     *
     * @return 终端
     */
    public static Terminal determineTerminal() {
        return determineTerminal(RequestUtil.getRequest());
    }

    /**
     * 确定终端，返回值可能为null。
     *
     * @param request HttpServletRequest
     * @return 终端
     */
    public static Terminal determineTerminal(HttpServletRequest request) {
        String header = request.getHeader(HttpHeaders.USER_AGENT);
        if (header == null) {
            return null;
        }

        UserAgent userAgent = UserAgent.parseUserAgentString(header);
        DeviceType deviceType = userAgent.getOperatingSystem().getDeviceType();

        if (deviceType == DeviceType.COMPUTER) {
            return Terminal.PC;
        } else if (deviceType == DeviceType.MOBILE) {
            return Terminal.WAP;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static <T extends UserDetails> T getPrincipal(Class<T> clz) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return null;
        }
        Object principal = auth.getPrincipal();
        if (principal == null || !clz.isInstance(principal)) {
            return null;
        }
        return (T) principal;
    }

    public static String getSessionId() {
        return RequestUtil.getSession().getId();
    }

    public static void main(String[] args) {
        System.out.println(RandomStringUtils.randomAlphabetic(4).toLowerCase());
    }

}
