package com.yc.common.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 *
 */
public class PageUtil {

    public static Pageable getPageable(Integer pno, Integer psize, Sort sort) {
        if(pno == null || pno < 0) {
            pno = 0;
        }
        if(psize == null || psize < 0) {
            psize = 10;
        } else if(psize > 200) {
            psize = 200;
        }

        return new PageRequest(pno, psize, sort);
    }

    public static Pageable getPageable(Integer pno, Integer psize) {
        return getPageable(psize, psize, null);
    }
}
