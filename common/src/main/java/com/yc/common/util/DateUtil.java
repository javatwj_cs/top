package com.yc.common.util;

import com.yc.common.exception.ReturnCode;
import com.yc.common.exception.ServiceException;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * 时间工具类
 */
public class DateUtil {

    private static Logger logger = LoggerFactory.getLogger(DateUtil.class);

    public final static String NORMAL_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static Date now() {
        return new Date();
    }

    /**
     * 获取当天零点时间。
     *
     * @return 时间
     */
    public static Date today() {
        return truncateToDay(now());
    }

    /**
     * 获取明天零点时间。
     *
     * @return 时间
     */
    public static Date tomorrow() {
        return tomorrow(now());
    }

    /**
     * 获取指定日期第二天零点时间。
     *
     * @return 时间
     */
    public static Date tomorrow(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.add(Calendar.DAY_OF_MONTH, 1);
        return new Date(c.getTimeInMillis());
    }


    /**
     * 获取指定日期前一天零点时间。
     *
     * @return 时间
     */
    public static Date yesterday(Date date) {
        if(date == null){
            date = now();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.add(Calendar.DAY_OF_MONTH, -1);
        return new Date(c.getTimeInMillis());
    }

    /**
     * 向下截取到指定日期零点。
     *
     * @param date 日期
     * @return 当天零点
     */
    public static Date truncateToDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.HOUR_OF_DAY, 0);
        return new Date(c.getTimeInMillis());
    }

    /**
     * 增加天数
     * @param date 时间
     * @param count 天数：正-加天数，负-减天数
     * @return 增减后的时间
     */
    public static Date addDay(Date date, int count) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        int day = c.get(Calendar.DAY_OF_MONTH);
        c.set(Calendar.DAY_OF_MONTH, day + count);
        return c.getTime();
    }

    /**
     * 获取某天的最后时间点：23点59分59秒
     * @param date
     * @return
     */
    public static Date dayEnd(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.MILLISECOND, 999);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.HOUR_OF_DAY, 23);
        return new Date(c.getTimeInMillis());
    }

    /**
     * 获取某周的起始时间点：星期一：0点0分0秒
     * @param date
     * @return
     */
    public static Date weekStart(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return new Date(c.getTimeInMillis());
    }

    /**
     * 获取某周的最后时间点：星期天23点59分59秒
     * @param date
     * @return
     */
    public static Date weekEnd(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.MILLISECOND, 999);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return new Date(c.getTimeInMillis());
    }

    /**
     * 获取时间（GMT-4）
     *
     * @param date
     * @param format
     * @return
     */
    public static String getDateGMT4(Date date, String format) {
        return DateFormatUtils.format(date, format, TimeZone.getTimeZone("GMT-4:00"));
    }

    /**
     * 获取时间（GMT-0）
     *
     * @param date
     * @param format
     * @return
     */
    public static String getDateGMT0(Date date, String format) {
        return DateFormatUtils.format(date, format, TimeZone.getTimeZone("GMT-0:00"));
    }

    /**
     * 获取时间（GMT-4）
     *
     * @param date 时间
     * @return yyyyMMdd
     */
    public static String getDateGMT4yyyyMMdd(Date date) {
        return getDateGMT4(date, "yyyyMMdd");
    }

    /**
     * 获取时间（GMT-4）
     *
     * @param date 时间
     * @return yyyyMMddHHmmss
     */
    public static String getDateGMT4yyyyMMddHHmmss(Date date) {
        return getDateGMT4(date, "yyyyMMddHHmmss");
    }

    /**
     * 获取时间（GMT-4）
     *
     * @param date 时间
     * @return yyyyMMddHHmmss
     */
    public static String getDateGMT4NormalPattern(Date date) {
        return getDateGMT4(date, NORMAL_PATTERN);
    }

    /**
     * 获取时间（GMT-0）
     *
     * @param date 时间
     * @return yyyyMMddHHmmss
     */
    public static String getDateGMT0yyyyMMddHHmmss(Date date) {
        return getDateGMT0(date, "yyyyMMddHHmmss");
    }

    /**
     * 比较两个时间间隔
     *
     * @param firstStr
     * @param secondStr
     * @return
     * @throws ParseException
     */
    public static long longOfTwoDate(String firstStr, String secondStr) throws ParseException {
        Date first = DateUtils.parseDate(firstStr, NORMAL_PATTERN);
        Date second = DateUtils.parseDate(secondStr, NORMAL_PATTERN);

        int days = Math.abs((int) ((first.getTime() - second.getTime()) / (1000 * 60 * 60 * 24)));
        return days;
    }

    public static Date getDayOfTime(String dateStr) {
        if (StringUtils.isEmpty(dateStr)) {
            return null;
        }
        try {
            return DateUtils.parseDate(dateStr, NORMAL_PATTERN);
        } catch (ParseException e) {
            throw new ServiceException(ReturnCode.CODE_40004, "时间");
        }
    }

    public static String getDayOfStartTime2Str(String dateStr) {
        if (StringUtils.isEmpty(dateStr)) {
            return null;
        }
        return dateStr.trim() + " 00:00:00";
    }

    public static String getDayOfEndTime2Str(String dateStr) {
        if (StringUtils.isEmpty(dateStr)) {
            return null;
        }
        return dateStr.trim() + " 23:59:59";
    }

    public static Date getDayOfStartTime(String dateStr) {
        if (StringUtils.isEmpty(dateStr)) {
            return null;
        }
        try {
            return DateUtils.parseDate(dateStr, "yyyy-MM-dd");
        } catch (ParseException e) {
            throw new ServiceException(ReturnCode.CODE_40004, "时间");
        }
    }

    public static Date getDayOfEndTime(String dateStr) {
        if (StringUtils.isEmpty(dateStr)) {
            return null;
        }
        try {
            Date date = DateUtils.parseDate(dateStr, "yyyy-MM-dd");
            date = DateUtils.setHours(date, 23);
            date = DateUtils.setMinutes(date, 59);
            date = DateUtils.setSeconds(date, 59);
            return date;
        } catch (ParseException e) {
            throw new ServiceException(ReturnCode.CODE_40004, "时间");
        }
    }

    public static Date strToDate(String dateStr, Date def) {
        if (StringUtils.isEmpty(dateStr)) {
            return def;
        }

        try {
            return DateUtils.parseDate(dateStr, DateUtil.NORMAL_PATTERN);
        } catch (Exception e) {
        }

        return def;
    }

    public static java.sql.Date getSqlDate(String dateStr) {
        if (StringUtils.isEmpty(dateStr)) {
            return null;
        }
        try {
            Date date = DateUtils.parseDate(dateStr, "yyyy-MM-dd");
            return new java.sql.Date(date.getTime());
        } catch (ParseException e) {
            throw new ServiceException(ReturnCode.CODE_40004, "时间");
        }
    }

    /**
     * 日期格式化
     * @param date
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String format(Date date) {
        return DateFormatUtils.format(date, NORMAL_PATTERN);
    }

    public static String getTimePeriod(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int month = c.get(Calendar.MONTH) + 1;
        int week = c.get(Calendar.WEEK_OF_MONTH);
        return month + "月第" + week + "周";
    }

    public static boolean notExpired(Date start, Date end) {
        Date now = now();
        return start.before(now) && end.after(now);
    }

    /**
     * 计算两个日期之间相差的天数
     * @param date1
     * @param date2
     * @return
     */
    public static Integer daysBetween(Date date1,Date date2)
    {
        Assert.notNull(date1, "比较时间参数1不能为空！");
        Assert.notNull(date2, "比较时间参数2不能为空！");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        long time1 = cal.getTimeInMillis();
        cal.setTime(date2);
        long time2 = cal.getTimeInMillis();
        long between_days=(time2-time1)/(1000*3600*24);

        return Integer.parseInt(String.valueOf(between_days));
    }
}
