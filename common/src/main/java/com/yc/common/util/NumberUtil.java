package com.yc.common.util;

import org.apache.commons.lang3.math.NumberUtils;

public class NumberUtil {

    /**
     * true:为0
     * @param value
     * @return true:为0
     */
    public static boolean isZero(Integer value) {
        return value == null || value.equals(NumberUtils.INTEGER_ZERO);
    }

    /**
     * true:为0
     * @param value
     * @return true:为0
     */
    public static boolean isZero(Float value) {
        return value == null || value.equals(NumberUtils.FLOAT_ZERO);
    }

    /**
     * true:为0
     * @param value
     * @return true:为0
     */
    public static boolean isZero(Long value) {
        return value == null || value.equals(NumberUtils.LONG_ZERO);
    }

    /**
     * true:不为0
     * @param value
     * @return true:为0
     */
    public static boolean isNotZero(Integer value) {
        return !isZero(value);
    }

    /**
     * true:不为0
     * @param value
     * @return true:为0
     */
    public static boolean isNotZero(Float value) {
        return !isZero(value);
    }
}
