package com.yc.common.util;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 */
public class StringUtil {
    private static AtomicInteger COUNTER = new AtomicInteger();

    public static String fluse2UrlStr(String url, Map<String, Object> params) {
        if (params == null || params.size() == 0) {
            return url;
        }
        StringBuilder sb = new StringBuilder();
        Iterator<String> iterator = params.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            sb = sb.append("&").append(key).append("=").append(params.get(key));
        }
        sb = sb.replace(0, 1, "?");
        return url + sb.toString();
    }

    public static String createTestUsername() {
        return String.valueOf(System.currentTimeMillis()).substring(4);
    }

    public static String getInviteCode() {
        return RandomStringUtils.random(5, "0123456789abcdefghijklmnopqrstuvwxyz");
    }

    /**
     * SQL LIKE clause escaper.
     * This SQL LIKE clause escaper does not protect against SQL injection, but ensure
     * that the string to be consumed in SQL LIKE clause does not alter the current
     * LIKE query by inserting <code>%</code> or <code>_</code>:
     * 
     * <pre>
     * entityManager.createQuery("FROM MyEntity e WHERE e.content LIKE :like_query ESCAPE '@'").setParameter("like_query", "%" + Escape.sqlLikeClause(USER_DATA_HERE)).getResultList();
     * </pre>
     * 
     * This escaper has to be used with a safe SQL query construct such as the JPQL
     * named parameterized query in the previous example.
     * This escaper uses by default the <code>@</code> as escape character. The other method
     * This SQL LIKE escaper processes the following characters:
     * <ul>
     * <li>
     * SQL LIKE characters: <code>_ (U+005F)</code>, <code>% (U+0025)</code>, <code>@ (U+0040)</code>
     * </li>
     * </ul>
     * StringUtils.replace(str, "'", "''");
     */
    public static String sqlLikeClause(String input) {
        char escape = '@';
        if (input == null)
            return null;

        int length = input.length();
        StringBuilder output = allocateStringBuilder(length);

        for (int i = 0; i < length; i++) {
            char c = input.charAt(i);
            if (c == escape || c == '_' || c == '%') {
                output.append(escape);
            }
            output.append(c);
        }
        return output.toString();
    }

    private static StringBuilder allocateStringBuilder(int length) {
        int buflen = length;
        if (length * 2 > 0)
            buflen = length * 2;
        return new StringBuilder(buflen);
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        /*Map<String, Object> params = new HashMap<>();
        params.put("propertyId", "5006070");
        params.put("data", "DiLNDOvSuYDM%2bQesCiB7YXDvJSM7gQ%2bsvJ6mYYC5T1MXeOZuk69OyA%3d%3d");
        params.put("sign", "CGHvj9Hgb905YuBXlN9kkA%3d%3d");
        
        String url = "https://api3.abgapi.net/query_handicap";
        
        System.out.println(fluse2UrlStr(url, params));*/

        // DiLNDOvSuYBOYIS3IDgcIPtgsrdFpNfZQoBAF3ncQKM=

        // System.out.println(URLEncoder.encode("Dmir9tNRgQeImQUCJrhU6Q==", "utf-8"));
        // System.out.println(new SecureRandom().nextLong());
        /*for (int i = 0; i < 50; i++) {
            System.out.println(RandomStringUtils.random(5, "0123456789abcdefghijklmnopqrstuvwxyz"));
        }*/
    }
}
