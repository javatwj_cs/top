package com.yc.common.exception;

/**
 *
 */
public enum ReturnCode {

    SUCCESS(1,"成功"),
    SERVER_EXCEPTION(500,"服务器异常，请稍后再试"),
    ERROR(500,"服务器异常，请稍后再试"),
    PAGE_NOT_FOUND(404,"请求不存在"),
    //1XXXX	数据库异常，XXXX为四位MYSQL的错误码

    //2XXXX 将EXCEPTION的首行信息作为MSG返回
    SYSTEM_EXCEPTION(20000,"JAVA系统级异常"),

    //3XXXX	安全类异常
//    REQUEST_FREQUENTLY(30001,"请求过于频繁，请稍后重试"),
//    REQUEST_NEED_HTTPS(30002,"本接口需要HTTPS来请求"),
//    COOKIE_INVALID(30003,"无效的COOKIE信息"),
//    REQUEST_INVALID(30004,"不合法的请求"),
//    USER_NOT_LOGIN(3005,"用户未登录"),
//    TOKEN_SIGN_INVALID(30006,"请求验证不通过"),
//    USER_CENTER_ERROR(30007,"请求用户中心发生问题"),
//    SIGN_ERROR(30008,"数据验签失败"),

    //4XXXX 网络级异常
    CODE_40001(40001, "手机异常，{0}"),
    CODE_40002(40002, "验证码异常，{0}"),
    CODE_40003(40003, "请选择，{0}"),
    CODE_40004(40004, "{0}，不合法"),
    CODE_40005(40005, "{0}，不存在"),
    CODE_40006(40006, "{0}，已存在"),
    CODE_40007(40007, "{0}，无效"),
    CODE_40008(40008, "{0}，错误"),


//    REQUEST_URL_OVER_LENGTH(40001,"请求的URL长度过长"),
//    REQUEST_ONLY_SUPPORT_GET_OR_POST(40002,"仅支持GET和POST方法，不支持其他方法"),
//    REQUEST_TIME_OUT(40003,"后台处理超时"),

    //500XX 通用的异常校验码
    CODE_50001(50001, "验证码异常，{0}"),

    // 50100 message 异常
    CODE_50100(50100, "消息异常，{0}"),
    CODE_50101(50101, "短信异常，{0}"),
    CODE_50102(50102, "邮箱异常，{0}"),
    CODE_50103(50103, "站内信异常，{0}"),

    // 50200 user异常
    CODE_50200(50200, "用户异常，{0}"),
    CODE_50201(50201, "登陆异常，{0}"),
    CODE_50202(50202, "更改用户信息异常，{0}"),

    // 50300 account异常
    CODE_50300(50300, "资金异常，{0}"),
    CODE_50301(50301, "XX币异常，{0}"),
    CODE_50302(50302, "冻结XX币异常，{0}"),
    CODE_50303(50303, "积分异常，{0}"),





//    REQUEST_JOSN_INVALID(50000,"请求消息JSON无法解析"),
//    REQUEST_PARAM_NOT_EXIST(50001,"参数不存在"),
//    REQUEST_PARAM_NOT_EMPTY(50002,"参数不能为空"),
//    CODE_40004(5001, "手机{0}");

    ;


    private int code;
    private String message;

    ReturnCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
