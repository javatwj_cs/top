package com.yc.common.exception;

import java.text.MessageFormat;

/**
 * 业务系统异常。
 */
public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = 7033612341626022783L;

    private int code;

    public ServiceException(ReturnCode returnCode, Object... args) {
        this(returnCode.getCode(), MessageFormat.format(returnCode.getMessage(), args));
    }

    public ServiceException(ReturnCode returnCode) {
        this(returnCode.getCode(), returnCode.getMessage());
    }

    public ServiceException(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
