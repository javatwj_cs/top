package com.yc.common.constant;

/**
 * Created by tianjie on 17/8/20.
 */
public enum AccountType {

    AMOUNT("XX币"),
    FREEZE_AMOUNT("冻结XX币"),
    INTEGRAL("积分"),
    ;

    AccountType(String subject) {
        this.subject = subject;
    }

    private String subject;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
