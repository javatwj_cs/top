package com.yc.common.constant;

/**
 * Created by tianjie on 17/7/16.
 */
/** 终端 */
public enum Terminal {

    PC,
    WAP,
    WX,
    ANDROID,
    IOS,
}
