package com.yc.common.constant;

/**
 * Created by tianjie on 17/8/20.
 */
public enum BillType {

    // axxyy
    // a:{1:"入款" 2:"出款", 3:"冻结", 4:"解冻"}
    // xx:{01:"资金", 01:"积分"}
    // yy:具体类型

    DEPOSIT(10101, "存款"),
    WITHDRAW(10201, "取款"),

    ;

    BillType(int code, String subject) {
        this.code = code;
        this.subject = subject;
    }

    private int code;
    private String subject;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
